﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class win_CréationFacture
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(win_CréationFacture))
        Me.tlp_Facture5 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_PrixTTCFacture5 = New System.Windows.Forms.Label()
        Me.lbl_QuantitéHeureFacture5 = New System.Windows.Forms.Label()
        Me.lbl_PrixUnitaireFacture5 = New System.Windows.Forms.Label()
        Me.lbl_DésignationFacture5 = New System.Windows.Forms.Label()
        Me.lbl_CodeFacture5 = New System.Windows.Forms.Label()
        Me.lbl_ÉchéanceFacture5 = New System.Windows.Forms.Label()
        Me.lbl_ModeRèglementFacture5 = New System.Windows.Forms.Label()
        Me.lbl_IdentitéClientFacture5 = New System.Windows.Forms.Label()
        Me.lbl_Facture5 = New System.Windows.Forms.Label()
        Me.lbl_Type5 = New System.Windows.Forms.Label()
        Me.lbl_Référence5 = New System.Windows.Forms.Label()
        Me.lbl_MoisFacture5 = New System.Windows.Forms.Label()
        Me.lbl_NuméroFacture5 = New System.Windows.Forms.Label()
        Me.lbl_TypeClientFacture5 = New System.Windows.Forms.Label()
        Me.lbl_NumClientFacture5 = New System.Windows.Forms.Label()
        Me.lbl_DateFacture5 = New System.Windows.Forms.Label()
        Me.dtp_Date5 = New System.Windows.Forms.DateTimePicker()
        Me.cbx_Mois5 = New System.Windows.Forms.ComboBox()
        Me.lbl_NomPrénom5 = New System.Windows.Forms.Label()
        Me.lbl_Échéance5 = New System.Windows.Forms.Label()
        Me.cbx_Règlement5 = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lbl_TotalTTC5 = New System.Windows.Forms.Label()
        Me.lbl_Prix15 = New System.Windows.Forms.Label()
        Me.lbl_Prix25 = New System.Windows.Forms.Label()
        Me.lbl_Prix35 = New System.Windows.Forms.Label()
        Me.lbl_Prix45 = New System.Windows.Forms.Label()
        Me.txb_QuantitéHeure15 = New System.Windows.Forms.TextBox()
        Me.txb_QuantitéHeure25 = New System.Windows.Forms.TextBox()
        Me.txb_QuantitéHeure35 = New System.Windows.Forms.TextBox()
        Me.txb_QuantitéHeure45 = New System.Windows.Forms.TextBox()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.txb_PrixUnitaire15 = New System.Windows.Forms.TextBox()
        Me.cbx_Majoration15 = New System.Windows.Forms.ComboBox()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.txb_PrixUnitaire25 = New System.Windows.Forms.TextBox()
        Me.cbx_Majoration25 = New System.Windows.Forms.ComboBox()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.txb_PrixUnitaire35 = New System.Windows.Forms.TextBox()
        Me.cbx_Majoration35 = New System.Windows.Forms.ComboBox()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.txb_PrixUnitaire45 = New System.Windows.Forms.TextBox()
        Me.cbx_Majoration45 = New System.Windows.Forms.ComboBox()
        Me.TableLayoutPanel5 = New System.Windows.Forms.TableLayoutPanel()
        Me.cbx_Objet15 = New System.Windows.Forms.ComboBox()
        Me.TableLayoutPanel6 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.dtp_PériodeDébut15 = New System.Windows.Forms.DateTimePicker()
        Me.dtp_PériodeFin15 = New System.Windows.Forms.DateTimePicker()
        Me.TableLayoutPanel7 = New System.Windows.Forms.TableLayoutPanel()
        Me.cbx_Objet25 = New System.Windows.Forms.ComboBox()
        Me.TableLayoutPanel8 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.dtp_PériodeDébut25 = New System.Windows.Forms.DateTimePicker()
        Me.dtp_PériodeFin25 = New System.Windows.Forms.DateTimePicker()
        Me.TableLayoutPanel11 = New System.Windows.Forms.TableLayoutPanel()
        Me.cbx_Objet35 = New System.Windows.Forms.ComboBox()
        Me.TableLayoutPanel12 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.dtp_PériodeDébut35 = New System.Windows.Forms.DateTimePicker()
        Me.dtp_PériodeFin35 = New System.Windows.Forms.DateTimePicker()
        Me.TableLayoutPanel9 = New System.Windows.Forms.TableLayoutPanel()
        Me.cbx_Objet45 = New System.Windows.Forms.ComboBox()
        Me.TableLayoutPanel10 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.dtp_PériodeDébut45 = New System.Windows.Forms.DateTimePicker()
        Me.dtp_PériodeFin45 = New System.Windows.Forms.DateTimePicker()
        Me.btn_Génération5 = New System.Windows.Forms.Button()
        Me.btn_Calculer5 = New System.Windows.Forms.Button()
        Me.tlp_Facture5.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.TableLayoutPanel5.SuspendLayout()
        Me.TableLayoutPanel6.SuspendLayout()
        Me.TableLayoutPanel7.SuspendLayout()
        Me.TableLayoutPanel8.SuspendLayout()
        Me.TableLayoutPanel11.SuspendLayout()
        Me.TableLayoutPanel12.SuspendLayout()
        Me.TableLayoutPanel9.SuspendLayout()
        Me.TableLayoutPanel10.SuspendLayout()
        Me.SuspendLayout()
        '
        'tlp_Facture5
        '
        Me.tlp_Facture5.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.tlp_Facture5.ColumnCount = 5
        Me.tlp_Facture5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.tlp_Facture5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 400.0!))
        Me.tlp_Facture5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200.0!))
        Me.tlp_Facture5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200.0!))
        Me.tlp_Facture5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 116.0!))
        Me.tlp_Facture5.Controls.Add(Me.lbl_PrixTTCFacture5, 4, 4)
        Me.tlp_Facture5.Controls.Add(Me.lbl_QuantitéHeureFacture5, 3, 4)
        Me.tlp_Facture5.Controls.Add(Me.lbl_PrixUnitaireFacture5, 2, 4)
        Me.tlp_Facture5.Controls.Add(Me.lbl_DésignationFacture5, 1, 4)
        Me.tlp_Facture5.Controls.Add(Me.lbl_CodeFacture5, 0, 4)
        Me.tlp_Facture5.Controls.Add(Me.lbl_ÉchéanceFacture5, 4, 2)
        Me.tlp_Facture5.Controls.Add(Me.lbl_ModeRèglementFacture5, 2, 2)
        Me.tlp_Facture5.Controls.Add(Me.lbl_IdentitéClientFacture5, 0, 2)
        Me.tlp_Facture5.Controls.Add(Me.lbl_Facture5, 3, 1)
        Me.tlp_Facture5.Controls.Add(Me.lbl_Type5, 2, 1)
        Me.tlp_Facture5.Controls.Add(Me.lbl_Référence5, 1, 1)
        Me.tlp_Facture5.Controls.Add(Me.lbl_MoisFacture5, 4, 0)
        Me.tlp_Facture5.Controls.Add(Me.lbl_NuméroFacture5, 3, 0)
        Me.tlp_Facture5.Controls.Add(Me.lbl_TypeClientFacture5, 2, 0)
        Me.tlp_Facture5.Controls.Add(Me.lbl_NumClientFacture5, 1, 0)
        Me.tlp_Facture5.Controls.Add(Me.lbl_DateFacture5, 0, 0)
        Me.tlp_Facture5.Controls.Add(Me.dtp_Date5, 0, 1)
        Me.tlp_Facture5.Controls.Add(Me.cbx_Mois5, 4, 1)
        Me.tlp_Facture5.Controls.Add(Me.lbl_NomPrénom5, 0, 3)
        Me.tlp_Facture5.Controls.Add(Me.lbl_Échéance5, 4, 3)
        Me.tlp_Facture5.Controls.Add(Me.cbx_Règlement5, 2, 3)
        Me.tlp_Facture5.Controls.Add(Me.Label1, 0, 5)
        Me.tlp_Facture5.Controls.Add(Me.Label2, 3, 9)
        Me.tlp_Facture5.Controls.Add(Me.lbl_TotalTTC5, 4, 9)
        Me.tlp_Facture5.Controls.Add(Me.lbl_Prix15, 4, 5)
        Me.tlp_Facture5.Controls.Add(Me.lbl_Prix25, 4, 6)
        Me.tlp_Facture5.Controls.Add(Me.lbl_Prix35, 4, 7)
        Me.tlp_Facture5.Controls.Add(Me.lbl_Prix45, 4, 8)
        Me.tlp_Facture5.Controls.Add(Me.txb_QuantitéHeure15, 3, 5)
        Me.tlp_Facture5.Controls.Add(Me.txb_QuantitéHeure25, 3, 6)
        Me.tlp_Facture5.Controls.Add(Me.txb_QuantitéHeure35, 3, 7)
        Me.tlp_Facture5.Controls.Add(Me.txb_QuantitéHeure45, 3, 8)
        Me.tlp_Facture5.Controls.Add(Me.TableLayoutPanel1, 2, 5)
        Me.tlp_Facture5.Controls.Add(Me.TableLayoutPanel2, 2, 6)
        Me.tlp_Facture5.Controls.Add(Me.TableLayoutPanel3, 2, 7)
        Me.tlp_Facture5.Controls.Add(Me.TableLayoutPanel4, 2, 8)
        Me.tlp_Facture5.Controls.Add(Me.TableLayoutPanel5, 1, 5)
        Me.tlp_Facture5.Controls.Add(Me.TableLayoutPanel7, 1, 6)
        Me.tlp_Facture5.Controls.Add(Me.TableLayoutPanel11, 1, 7)
        Me.tlp_Facture5.Controls.Add(Me.TableLayoutPanel9, 1, 8)
        Me.tlp_Facture5.Location = New System.Drawing.Point(2, 2)
        Me.tlp_Facture5.Name = "tlp_Facture5"
        Me.tlp_Facture5.RowCount = 10
        Me.tlp_Facture5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.tlp_Facture5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tlp_Facture5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.tlp_Facture5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tlp_Facture5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.tlp_Facture5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 93.0!))
        Me.tlp_Facture5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.tlp_Facture5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 108.0!))
        Me.tlp_Facture5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 83.0!))
        Me.tlp_Facture5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8.0!))
        Me.tlp_Facture5.Size = New System.Drawing.Size(1010, 607)
        Me.tlp_Facture5.TabIndex = 0
        '
        'lbl_PrixTTCFacture5
        '
        Me.lbl_PrixTTCFacture5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_PrixTTCFacture5.AutoSize = True
        Me.lbl_PrixTTCFacture5.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_PrixTTCFacture5.Location = New System.Drawing.Point(908, 137)
        Me.lbl_PrixTTCFacture5.Name = "lbl_PrixTTCFacture5"
        Me.lbl_PrixTTCFacture5.Size = New System.Drawing.Size(110, 40)
        Me.lbl_PrixTTCFacture5.TabIndex = 27
        Me.lbl_PrixTTCFacture5.Text = "Prix TTC"
        Me.lbl_PrixTTCFacture5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_QuantitéHeureFacture5
        '
        Me.lbl_QuantitéHeureFacture5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_QuantitéHeureFacture5.AutoSize = True
        Me.lbl_QuantitéHeureFacture5.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_QuantitéHeureFacture5.Location = New System.Drawing.Point(707, 137)
        Me.lbl_QuantitéHeureFacture5.Name = "lbl_QuantitéHeureFacture5"
        Me.lbl_QuantitéHeureFacture5.Size = New System.Drawing.Size(194, 40)
        Me.lbl_QuantitéHeureFacture5.TabIndex = 26
        Me.lbl_QuantitéHeureFacture5.Text = "Quantité en heure"
        Me.lbl_QuantitéHeureFacture5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_PrixUnitaireFacture5
        '
        Me.lbl_PrixUnitaireFacture5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_PrixUnitaireFacture5.AutoSize = True
        Me.lbl_PrixUnitaireFacture5.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_PrixUnitaireFacture5.Location = New System.Drawing.Point(506, 137)
        Me.lbl_PrixUnitaireFacture5.Name = "lbl_PrixUnitaireFacture5"
        Me.lbl_PrixUnitaireFacture5.Size = New System.Drawing.Size(194, 40)
        Me.lbl_PrixUnitaireFacture5.TabIndex = 25
        Me.lbl_PrixUnitaireFacture5.Text = "Prix unitaire TTC"
        Me.lbl_PrixUnitaireFacture5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_DésignationFacture5
        '
        Me.lbl_DésignationFacture5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_DésignationFacture5.AutoSize = True
        Me.lbl_DésignationFacture5.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_DésignationFacture5.Location = New System.Drawing.Point(105, 137)
        Me.lbl_DésignationFacture5.Name = "lbl_DésignationFacture5"
        Me.lbl_DésignationFacture5.Size = New System.Drawing.Size(394, 40)
        Me.lbl_DésignationFacture5.TabIndex = 24
        Me.lbl_DésignationFacture5.Text = "Désignation"
        Me.lbl_DésignationFacture5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_CodeFacture5
        '
        Me.lbl_CodeFacture5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_CodeFacture5.AutoSize = True
        Me.lbl_CodeFacture5.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_CodeFacture5.Location = New System.Drawing.Point(4, 137)
        Me.lbl_CodeFacture5.Name = "lbl_CodeFacture5"
        Me.lbl_CodeFacture5.Size = New System.Drawing.Size(94, 40)
        Me.lbl_CodeFacture5.TabIndex = 23
        Me.lbl_CodeFacture5.Text = "Code"
        Me.lbl_CodeFacture5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_ÉchéanceFacture5
        '
        Me.lbl_ÉchéanceFacture5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_ÉchéanceFacture5.AutoSize = True
        Me.lbl_ÉchéanceFacture5.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ÉchéanceFacture5.Location = New System.Drawing.Point(908, 69)
        Me.lbl_ÉchéanceFacture5.Name = "lbl_ÉchéanceFacture5"
        Me.lbl_ÉchéanceFacture5.Size = New System.Drawing.Size(110, 40)
        Me.lbl_ÉchéanceFacture5.TabIndex = 18
        Me.lbl_ÉchéanceFacture5.Text = "Échéance"
        Me.lbl_ÉchéanceFacture5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_ModeRèglementFacture5
        '
        Me.lbl_ModeRèglementFacture5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_ModeRèglementFacture5.AutoSize = True
        Me.tlp_Facture5.SetColumnSpan(Me.lbl_ModeRèglementFacture5, 2)
        Me.lbl_ModeRèglementFacture5.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ModeRèglementFacture5.Location = New System.Drawing.Point(506, 69)
        Me.lbl_ModeRèglementFacture5.Name = "lbl_ModeRèglementFacture5"
        Me.lbl_ModeRèglementFacture5.Size = New System.Drawing.Size(395, 40)
        Me.lbl_ModeRèglementFacture5.TabIndex = 16
        Me.lbl_ModeRèglementFacture5.Text = "Mode de règlement"
        Me.lbl_ModeRèglementFacture5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_IdentitéClientFacture5
        '
        Me.lbl_IdentitéClientFacture5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_IdentitéClientFacture5.AutoSize = True
        Me.tlp_Facture5.SetColumnSpan(Me.lbl_IdentitéClientFacture5, 2)
        Me.lbl_IdentitéClientFacture5.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_IdentitéClientFacture5.Location = New System.Drawing.Point(4, 69)
        Me.lbl_IdentitéClientFacture5.Name = "lbl_IdentitéClientFacture5"
        Me.lbl_IdentitéClientFacture5.Size = New System.Drawing.Size(495, 40)
        Me.lbl_IdentitéClientFacture5.TabIndex = 14
        Me.lbl_IdentitéClientFacture5.Text = "Référence client"
        Me.lbl_IdentitéClientFacture5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_Facture5
        '
        Me.lbl_Facture5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_Facture5.AutoSize = True
        Me.lbl_Facture5.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Facture5.Location = New System.Drawing.Point(707, 42)
        Me.lbl_Facture5.Name = "lbl_Facture5"
        Me.lbl_Facture5.Size = New System.Drawing.Size(194, 26)
        Me.lbl_Facture5.TabIndex = 12
        Me.lbl_Facture5.Text = "-"
        Me.lbl_Facture5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_Type5
        '
        Me.lbl_Type5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_Type5.AutoSize = True
        Me.lbl_Type5.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Type5.Location = New System.Drawing.Point(506, 42)
        Me.lbl_Type5.Name = "lbl_Type5"
        Me.lbl_Type5.Size = New System.Drawing.Size(194, 26)
        Me.lbl_Type5.TabIndex = 11
        Me.lbl_Type5.Text = "-"
        Me.lbl_Type5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_Référence5
        '
        Me.lbl_Référence5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_Référence5.AutoSize = True
        Me.lbl_Référence5.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Référence5.Location = New System.Drawing.Point(105, 42)
        Me.lbl_Référence5.Name = "lbl_Référence5"
        Me.lbl_Référence5.Size = New System.Drawing.Size(394, 26)
        Me.lbl_Référence5.TabIndex = 10
        Me.lbl_Référence5.Text = "-"
        Me.lbl_Référence5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_MoisFacture5
        '
        Me.lbl_MoisFacture5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_MoisFacture5.AutoSize = True
        Me.lbl_MoisFacture5.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_MoisFacture5.Location = New System.Drawing.Point(908, 1)
        Me.lbl_MoisFacture5.Name = "lbl_MoisFacture5"
        Me.lbl_MoisFacture5.Size = New System.Drawing.Size(110, 40)
        Me.lbl_MoisFacture5.TabIndex = 8
        Me.lbl_MoisFacture5.Text = "Mois"
        Me.lbl_MoisFacture5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_NuméroFacture5
        '
        Me.lbl_NuméroFacture5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_NuméroFacture5.AutoSize = True
        Me.lbl_NuméroFacture5.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_NuméroFacture5.Location = New System.Drawing.Point(707, 1)
        Me.lbl_NuméroFacture5.Name = "lbl_NuméroFacture5"
        Me.lbl_NuméroFacture5.Size = New System.Drawing.Size(194, 40)
        Me.lbl_NuméroFacture5.TabIndex = 7
        Me.lbl_NuméroFacture5.Text = "Facture"
        Me.lbl_NuméroFacture5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_TypeClientFacture5
        '
        Me.lbl_TypeClientFacture5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_TypeClientFacture5.AutoSize = True
        Me.lbl_TypeClientFacture5.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_TypeClientFacture5.Location = New System.Drawing.Point(506, 1)
        Me.lbl_TypeClientFacture5.Name = "lbl_TypeClientFacture5"
        Me.lbl_TypeClientFacture5.Size = New System.Drawing.Size(194, 40)
        Me.lbl_TypeClientFacture5.TabIndex = 6
        Me.lbl_TypeClientFacture5.Text = "Type client"
        Me.lbl_TypeClientFacture5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_NumClientFacture5
        '
        Me.lbl_NumClientFacture5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_NumClientFacture5.AutoSize = True
        Me.lbl_NumClientFacture5.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_NumClientFacture5.Location = New System.Drawing.Point(105, 1)
        Me.lbl_NumClientFacture5.Name = "lbl_NumClientFacture5"
        Me.lbl_NumClientFacture5.Size = New System.Drawing.Size(394, 40)
        Me.lbl_NumClientFacture5.TabIndex = 5
        Me.lbl_NumClientFacture5.Text = "N° Client"
        Me.lbl_NumClientFacture5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_DateFacture5
        '
        Me.lbl_DateFacture5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_DateFacture5.AutoSize = True
        Me.lbl_DateFacture5.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_DateFacture5.Location = New System.Drawing.Point(4, 1)
        Me.lbl_DateFacture5.Name = "lbl_DateFacture5"
        Me.lbl_DateFacture5.Size = New System.Drawing.Size(94, 40)
        Me.lbl_DateFacture5.TabIndex = 4
        Me.lbl_DateFacture5.Text = "Date"
        Me.lbl_DateFacture5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dtp_Date5
        '
        Me.dtp_Date5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtp_Date5.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_Date5.Location = New System.Drawing.Point(4, 45)
        Me.dtp_Date5.Name = "dtp_Date5"
        Me.dtp_Date5.Size = New System.Drawing.Size(94, 20)
        Me.dtp_Date5.TabIndex = 9
        '
        'cbx_Mois5
        '
        Me.cbx_Mois5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbx_Mois5.FormattingEnabled = True
        Me.cbx_Mois5.Items.AddRange(New Object() {"JANVIER", "FÉVRIER", "MARS", "AVRIL", "MAI", "JUIN", "JUILLET", "AOÛT", "SEPTEMBRE", "OCTOBRE", "NOVEMBRE", "DÉCEMBRE"})
        Me.cbx_Mois5.Location = New System.Drawing.Point(908, 45)
        Me.cbx_Mois5.Name = "cbx_Mois5"
        Me.cbx_Mois5.Size = New System.Drawing.Size(98, 21)
        Me.cbx_Mois5.TabIndex = 13
        '
        'lbl_NomPrénom5
        '
        Me.lbl_NomPrénom5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_NomPrénom5.AutoSize = True
        Me.tlp_Facture5.SetColumnSpan(Me.lbl_NomPrénom5, 2)
        Me.lbl_NomPrénom5.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_NomPrénom5.Location = New System.Drawing.Point(4, 110)
        Me.lbl_NomPrénom5.Name = "lbl_NomPrénom5"
        Me.lbl_NomPrénom5.Size = New System.Drawing.Size(495, 26)
        Me.lbl_NomPrénom5.TabIndex = 19
        Me.lbl_NomPrénom5.Text = "-"
        Me.lbl_NomPrénom5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_Échéance5
        '
        Me.lbl_Échéance5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_Échéance5.AutoSize = True
        Me.lbl_Échéance5.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Échéance5.Location = New System.Drawing.Point(908, 110)
        Me.lbl_Échéance5.Name = "lbl_Échéance5"
        Me.lbl_Échéance5.Size = New System.Drawing.Size(110, 26)
        Me.lbl_Échéance5.TabIndex = 21
        Me.lbl_Échéance5.Text = "8 jours"
        Me.lbl_Échéance5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cbx_Règlement5
        '
        Me.tlp_Facture5.SetColumnSpan(Me.cbx_Règlement5, 2)
        Me.cbx_Règlement5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbx_Règlement5.FormattingEnabled = True
        Me.cbx_Règlement5.Items.AddRange(New Object() {"Virement", "Chèque", "CESU", "Chèque + CESU"})
        Me.cbx_Règlement5.Location = New System.Drawing.Point(608, 113)
        Me.cbx_Règlement5.Margin = New System.Windows.Forms.Padding(105, 3, 3, 3)
        Me.cbx_Règlement5.Name = "cbx_Règlement5"
        Me.cbx_Règlement5.Size = New System.Drawing.Size(194, 21)
        Me.cbx_Règlement5.TabIndex = 22
        '
        'Label1
        '
        Me.Label1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(4, 183)
        Me.Label1.Margin = New System.Windows.Forms.Padding(3, 5, 3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(94, 88)
        Me.Label1.TabIndex = 30
        Me.Label1.Text = "Objet :"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label2
        '
        Me.Label2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(707, 566)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(194, 40)
        Me.Label2.TabIndex = 31
        Me.Label2.Text = "TOTAL TTC (€)"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_TotalTTC5
        '
        Me.lbl_TotalTTC5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_TotalTTC5.AutoSize = True
        Me.lbl_TotalTTC5.Font = New System.Drawing.Font("Georgia", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_TotalTTC5.Location = New System.Drawing.Point(908, 566)
        Me.lbl_TotalTTC5.Name = "lbl_TotalTTC5"
        Me.lbl_TotalTTC5.Size = New System.Drawing.Size(110, 40)
        Me.lbl_TotalTTC5.TabIndex = 32
        Me.lbl_TotalTTC5.Text = "-"
        Me.lbl_TotalTTC5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_Prix15
        '
        Me.lbl_Prix15.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_Prix15.AutoSize = True
        Me.lbl_Prix15.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Prix15.Location = New System.Drawing.Point(908, 178)
        Me.lbl_Prix15.Name = "lbl_Prix15"
        Me.lbl_Prix15.Size = New System.Drawing.Size(110, 93)
        Me.lbl_Prix15.TabIndex = 33
        Me.lbl_Prix15.Text = "-"
        Me.lbl_Prix15.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lbl_Prix25
        '
        Me.lbl_Prix25.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_Prix25.AutoSize = True
        Me.lbl_Prix25.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Prix25.Location = New System.Drawing.Point(908, 272)
        Me.lbl_Prix25.Name = "lbl_Prix25"
        Me.lbl_Prix25.Size = New System.Drawing.Size(110, 100)
        Me.lbl_Prix25.TabIndex = 34
        Me.lbl_Prix25.Text = "-"
        Me.lbl_Prix25.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lbl_Prix35
        '
        Me.lbl_Prix35.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_Prix35.AutoSize = True
        Me.lbl_Prix35.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Prix35.Location = New System.Drawing.Point(908, 373)
        Me.lbl_Prix35.Name = "lbl_Prix35"
        Me.lbl_Prix35.Size = New System.Drawing.Size(110, 108)
        Me.lbl_Prix35.TabIndex = 35
        Me.lbl_Prix35.Text = "-"
        Me.lbl_Prix35.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lbl_Prix45
        '
        Me.lbl_Prix45.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_Prix45.AutoSize = True
        Me.lbl_Prix45.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Prix45.Location = New System.Drawing.Point(908, 482)
        Me.lbl_Prix45.Name = "lbl_Prix45"
        Me.lbl_Prix45.Size = New System.Drawing.Size(110, 83)
        Me.lbl_Prix45.TabIndex = 36
        Me.lbl_Prix45.Text = "-"
        Me.lbl_Prix45.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txb_QuantitéHeure15
        '
        Me.txb_QuantitéHeure15.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txb_QuantitéHeure15.Enabled = False
        Me.txb_QuantitéHeure15.Location = New System.Drawing.Point(759, 181)
        Me.txb_QuantitéHeure15.Margin = New System.Windows.Forms.Padding(55, 3, 55, 3)
        Me.txb_QuantitéHeure15.Name = "txb_QuantitéHeure15"
        Me.txb_QuantitéHeure15.Size = New System.Drawing.Size(90, 20)
        Me.txb_QuantitéHeure15.TabIndex = 37
        '
        'txb_QuantitéHeure25
        '
        Me.txb_QuantitéHeure25.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txb_QuantitéHeure25.Enabled = False
        Me.txb_QuantitéHeure25.Location = New System.Drawing.Point(759, 275)
        Me.txb_QuantitéHeure25.Margin = New System.Windows.Forms.Padding(55, 3, 55, 3)
        Me.txb_QuantitéHeure25.Name = "txb_QuantitéHeure25"
        Me.txb_QuantitéHeure25.Size = New System.Drawing.Size(90, 20)
        Me.txb_QuantitéHeure25.TabIndex = 38
        '
        'txb_QuantitéHeure35
        '
        Me.txb_QuantitéHeure35.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txb_QuantitéHeure35.Enabled = False
        Me.txb_QuantitéHeure35.Location = New System.Drawing.Point(759, 376)
        Me.txb_QuantitéHeure35.Margin = New System.Windows.Forms.Padding(55, 3, 55, 3)
        Me.txb_QuantitéHeure35.Name = "txb_QuantitéHeure35"
        Me.txb_QuantitéHeure35.Size = New System.Drawing.Size(90, 20)
        Me.txb_QuantitéHeure35.TabIndex = 39
        '
        'txb_QuantitéHeure45
        '
        Me.txb_QuantitéHeure45.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txb_QuantitéHeure45.Enabled = False
        Me.txb_QuantitéHeure45.Location = New System.Drawing.Point(759, 485)
        Me.txb_QuantitéHeure45.Margin = New System.Windows.Forms.Padding(55, 3, 55, 3)
        Me.txb_QuantitéHeure45.Name = "txb_QuantitéHeure45"
        Me.txb_QuantitéHeure45.Size = New System.Drawing.Size(90, 20)
        Me.txb_QuantitéHeure45.TabIndex = 40
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.txb_PrixUnitaire15, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.cbx_Majoration15, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(506, 181)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(194, 87)
        Me.TableLayoutPanel1.TabIndex = 41
        '
        'txb_PrixUnitaire15
        '
        Me.txb_PrixUnitaire15.Enabled = False
        Me.txb_PrixUnitaire15.Location = New System.Drawing.Point(3, 3)
        Me.txb_PrixUnitaire15.Name = "txb_PrixUnitaire15"
        Me.txb_PrixUnitaire15.Size = New System.Drawing.Size(91, 20)
        Me.txb_PrixUnitaire15.TabIndex = 0
        '
        'cbx_Majoration15
        '
        Me.cbx_Majoration15.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbx_Majoration15.Enabled = False
        Me.cbx_Majoration15.FormattingEnabled = True
        Me.cbx_Majoration15.Items.AddRange(New Object() {"", "Majoration 5%", "Majoration 10%", "Majoration 25%"})
        Me.cbx_Majoration15.Location = New System.Drawing.Point(100, 3)
        Me.cbx_Majoration15.Name = "cbx_Majoration15"
        Me.cbx_Majoration15.Size = New System.Drawing.Size(91, 21)
        Me.cbx_Majoration15.TabIndex = 1
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.txb_PrixUnitaire25, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.cbx_Majoration25, 1, 0)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(506, 275)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(194, 94)
        Me.TableLayoutPanel2.TabIndex = 42
        '
        'txb_PrixUnitaire25
        '
        Me.txb_PrixUnitaire25.Enabled = False
        Me.txb_PrixUnitaire25.Location = New System.Drawing.Point(3, 3)
        Me.txb_PrixUnitaire25.Name = "txb_PrixUnitaire25"
        Me.txb_PrixUnitaire25.Size = New System.Drawing.Size(91, 20)
        Me.txb_PrixUnitaire25.TabIndex = 0
        '
        'cbx_Majoration25
        '
        Me.cbx_Majoration25.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbx_Majoration25.Enabled = False
        Me.cbx_Majoration25.FormattingEnabled = True
        Me.cbx_Majoration25.Items.AddRange(New Object() {"", "Majoration 5%", "Majoration 10%", "Majoration 25%"})
        Me.cbx_Majoration25.Location = New System.Drawing.Point(100, 3)
        Me.cbx_Majoration25.Name = "cbx_Majoration25"
        Me.cbx_Majoration25.Size = New System.Drawing.Size(91, 21)
        Me.cbx_Majoration25.TabIndex = 1
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 2
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.txb_PrixUnitaire35, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.cbx_Majoration35, 1, 0)
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(506, 376)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 1
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(194, 100)
        Me.TableLayoutPanel3.TabIndex = 43
        '
        'txb_PrixUnitaire35
        '
        Me.txb_PrixUnitaire35.Enabled = False
        Me.txb_PrixUnitaire35.Location = New System.Drawing.Point(3, 3)
        Me.txb_PrixUnitaire35.Name = "txb_PrixUnitaire35"
        Me.txb_PrixUnitaire35.Size = New System.Drawing.Size(91, 20)
        Me.txb_PrixUnitaire35.TabIndex = 0
        '
        'cbx_Majoration35
        '
        Me.cbx_Majoration35.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbx_Majoration35.Enabled = False
        Me.cbx_Majoration35.FormattingEnabled = True
        Me.cbx_Majoration35.Items.AddRange(New Object() {"", "Majoration 5%", "Majoration 10%", "Majoration 25%"})
        Me.cbx_Majoration35.Location = New System.Drawing.Point(100, 3)
        Me.cbx_Majoration35.Name = "cbx_Majoration35"
        Me.cbx_Majoration35.Size = New System.Drawing.Size(91, 21)
        Me.cbx_Majoration35.TabIndex = 1
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 2
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.txb_PrixUnitaire45, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.cbx_Majoration45, 1, 0)
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(506, 485)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(194, 77)
        Me.TableLayoutPanel4.TabIndex = 44
        '
        'txb_PrixUnitaire45
        '
        Me.txb_PrixUnitaire45.Enabled = False
        Me.txb_PrixUnitaire45.Location = New System.Drawing.Point(3, 3)
        Me.txb_PrixUnitaire45.Name = "txb_PrixUnitaire45"
        Me.txb_PrixUnitaire45.Size = New System.Drawing.Size(91, 20)
        Me.txb_PrixUnitaire45.TabIndex = 0
        '
        'cbx_Majoration45
        '
        Me.cbx_Majoration45.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbx_Majoration45.Enabled = False
        Me.cbx_Majoration45.FormattingEnabled = True
        Me.cbx_Majoration45.Items.AddRange(New Object() {"", "Majoration 5%", "Majoration 10%", "Majoration 25%"})
        Me.cbx_Majoration45.Location = New System.Drawing.Point(100, 3)
        Me.cbx_Majoration45.Name = "cbx_Majoration45"
        Me.cbx_Majoration45.Size = New System.Drawing.Size(91, 21)
        Me.cbx_Majoration45.TabIndex = 1
        '
        'TableLayoutPanel5
        '
        Me.TableLayoutPanel5.ColumnCount = 1
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel5.Controls.Add(Me.cbx_Objet15, 0, 0)
        Me.TableLayoutPanel5.Controls.Add(Me.TableLayoutPanel6, 0, 1)
        Me.TableLayoutPanel5.Location = New System.Drawing.Point(105, 181)
        Me.TableLayoutPanel5.Name = "TableLayoutPanel5"
        Me.TableLayoutPanel5.RowCount = 2
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 31.03448!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 68.96552!))
        Me.TableLayoutPanel5.Size = New System.Drawing.Size(394, 87)
        Me.TableLayoutPanel5.TabIndex = 45
        '
        'cbx_Objet15
        '
        Me.cbx_Objet15.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbx_Objet15.FormattingEnabled = True
        Me.cbx_Objet15.Items.AddRange(New Object() {"AIDE A LA PERSONNE", "MENAGE", "AUTRE"})
        Me.cbx_Objet15.Location = New System.Drawing.Point(3, 3)
        Me.cbx_Objet15.Name = "cbx_Objet15"
        Me.cbx_Objet15.Size = New System.Drawing.Size(196, 21)
        Me.cbx_Objet15.TabIndex = 0
        '
        'TableLayoutPanel6
        '
        Me.TableLayoutPanel6.ColumnCount = 4
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 94.0!))
        Me.TableLayoutPanel6.Controls.Add(Me.Label8, 0, 0)
        Me.TableLayoutPanel6.Controls.Add(Me.Label9, 2, 0)
        Me.TableLayoutPanel6.Controls.Add(Me.dtp_PériodeDébut15, 1, 0)
        Me.TableLayoutPanel6.Controls.Add(Me.dtp_PériodeFin15, 3, 0)
        Me.TableLayoutPanel6.Location = New System.Drawing.Point(3, 29)
        Me.TableLayoutPanel6.Name = "TableLayoutPanel6"
        Me.TableLayoutPanel6.RowCount = 1
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel6.Size = New System.Drawing.Size(388, 54)
        Me.TableLayoutPanel6.TabIndex = 1
        '
        'Label8
        '
        Me.Label8.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(3, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(91, 54)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Période du"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label9
        '
        Me.Label9.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(197, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(94, 54)
        Me.Label9.TabIndex = 1
        Me.Label9.Text = "au"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dtp_PériodeDébut15
        '
        Me.dtp_PériodeDébut15.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtp_PériodeDébut15.Enabled = False
        Me.dtp_PériodeDébut15.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_PériodeDébut15.Location = New System.Drawing.Point(100, 15)
        Me.dtp_PériodeDébut15.Margin = New System.Windows.Forms.Padding(3, 15, 3, 3)
        Me.dtp_PériodeDébut15.Name = "dtp_PériodeDébut15"
        Me.dtp_PériodeDébut15.Size = New System.Drawing.Size(91, 20)
        Me.dtp_PériodeDébut15.TabIndex = 2
        '
        'dtp_PériodeFin15
        '
        Me.dtp_PériodeFin15.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtp_PériodeFin15.Enabled = False
        Me.dtp_PériodeFin15.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_PériodeFin15.Location = New System.Drawing.Point(297, 15)
        Me.dtp_PériodeFin15.Margin = New System.Windows.Forms.Padding(3, 15, 3, 3)
        Me.dtp_PériodeFin15.Name = "dtp_PériodeFin15"
        Me.dtp_PériodeFin15.Size = New System.Drawing.Size(88, 20)
        Me.dtp_PériodeFin15.TabIndex = 3
        '
        'TableLayoutPanel7
        '
        Me.TableLayoutPanel7.ColumnCount = 1
        Me.TableLayoutPanel7.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel7.Controls.Add(Me.cbx_Objet25, 0, 0)
        Me.TableLayoutPanel7.Controls.Add(Me.TableLayoutPanel8, 0, 1)
        Me.TableLayoutPanel7.Location = New System.Drawing.Point(105, 275)
        Me.TableLayoutPanel7.Name = "TableLayoutPanel7"
        Me.TableLayoutPanel7.RowCount = 3
        Me.TableLayoutPanel7.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35.89743!))
        Me.TableLayoutPanel7.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 64.10256!))
        Me.TableLayoutPanel7.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8.0!))
        Me.TableLayoutPanel7.Size = New System.Drawing.Size(394, 87)
        Me.TableLayoutPanel7.TabIndex = 46
        '
        'cbx_Objet25
        '
        Me.cbx_Objet25.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbx_Objet25.FormattingEnabled = True
        Me.cbx_Objet25.Items.AddRange(New Object() {"AIDE A LA PERSONNE", "MENAGE", "AUTRE"})
        Me.cbx_Objet25.Location = New System.Drawing.Point(3, 3)
        Me.cbx_Objet25.Name = "cbx_Objet25"
        Me.cbx_Objet25.Size = New System.Drawing.Size(196, 21)
        Me.cbx_Objet25.TabIndex = 0
        '
        'TableLayoutPanel8
        '
        Me.TableLayoutPanel8.ColumnCount = 4
        Me.TableLayoutPanel8.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel8.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel8.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TableLayoutPanel8.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 94.0!))
        Me.TableLayoutPanel8.Controls.Add(Me.Label10, 0, 0)
        Me.TableLayoutPanel8.Controls.Add(Me.Label11, 2, 0)
        Me.TableLayoutPanel8.Controls.Add(Me.dtp_PériodeDébut25, 1, 0)
        Me.TableLayoutPanel8.Controls.Add(Me.dtp_PériodeFin25, 3, 0)
        Me.TableLayoutPanel8.Location = New System.Drawing.Point(3, 31)
        Me.TableLayoutPanel8.Name = "TableLayoutPanel8"
        Me.TableLayoutPanel8.RowCount = 1
        Me.TableLayoutPanel8.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel8.Size = New System.Drawing.Size(388, 44)
        Me.TableLayoutPanel8.TabIndex = 1
        '
        'Label10
        '
        Me.Label10.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(3, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(91, 44)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Période du"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label11
        '
        Me.Label11.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(197, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(94, 44)
        Me.Label11.TabIndex = 1
        Me.Label11.Text = "au"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dtp_PériodeDébut25
        '
        Me.dtp_PériodeDébut25.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtp_PériodeDébut25.Enabled = False
        Me.dtp_PériodeDébut25.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_PériodeDébut25.Location = New System.Drawing.Point(100, 10)
        Me.dtp_PériodeDébut25.Margin = New System.Windows.Forms.Padding(3, 10, 3, 3)
        Me.dtp_PériodeDébut25.Name = "dtp_PériodeDébut25"
        Me.dtp_PériodeDébut25.Size = New System.Drawing.Size(91, 20)
        Me.dtp_PériodeDébut25.TabIndex = 2
        '
        'dtp_PériodeFin25
        '
        Me.dtp_PériodeFin25.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtp_PériodeFin25.Enabled = False
        Me.dtp_PériodeFin25.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_PériodeFin25.Location = New System.Drawing.Point(297, 10)
        Me.dtp_PériodeFin25.Margin = New System.Windows.Forms.Padding(3, 10, 3, 3)
        Me.dtp_PériodeFin25.Name = "dtp_PériodeFin25"
        Me.dtp_PériodeFin25.Size = New System.Drawing.Size(88, 20)
        Me.dtp_PériodeFin25.TabIndex = 3
        '
        'TableLayoutPanel11
        '
        Me.TableLayoutPanel11.ColumnCount = 1
        Me.TableLayoutPanel11.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel11.Controls.Add(Me.cbx_Objet35, 0, 0)
        Me.TableLayoutPanel11.Controls.Add(Me.TableLayoutPanel12, 0, 1)
        Me.TableLayoutPanel11.Location = New System.Drawing.Point(105, 376)
        Me.TableLayoutPanel11.Name = "TableLayoutPanel11"
        Me.TableLayoutPanel11.RowCount = 2
        Me.TableLayoutPanel11.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 32.18391!))
        Me.TableLayoutPanel11.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 67.81609!))
        Me.TableLayoutPanel11.Size = New System.Drawing.Size(394, 87)
        Me.TableLayoutPanel11.TabIndex = 47
        '
        'cbx_Objet35
        '
        Me.cbx_Objet35.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbx_Objet35.FormattingEnabled = True
        Me.cbx_Objet35.Items.AddRange(New Object() {"AIDE A LA PERSONNE", "MENAGE", "AUTRE"})
        Me.cbx_Objet35.Location = New System.Drawing.Point(3, 3)
        Me.cbx_Objet35.Name = "cbx_Objet35"
        Me.cbx_Objet35.Size = New System.Drawing.Size(196, 21)
        Me.cbx_Objet35.TabIndex = 0
        '
        'TableLayoutPanel12
        '
        Me.TableLayoutPanel12.ColumnCount = 4
        Me.TableLayoutPanel12.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel12.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel12.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TableLayoutPanel12.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 94.0!))
        Me.TableLayoutPanel12.Controls.Add(Me.Label14, 0, 0)
        Me.TableLayoutPanel12.Controls.Add(Me.Label15, 2, 0)
        Me.TableLayoutPanel12.Controls.Add(Me.dtp_PériodeDébut35, 1, 0)
        Me.TableLayoutPanel12.Controls.Add(Me.dtp_PériodeFin35, 3, 0)
        Me.TableLayoutPanel12.Location = New System.Drawing.Point(3, 31)
        Me.TableLayoutPanel12.Name = "TableLayoutPanel12"
        Me.TableLayoutPanel12.RowCount = 1
        Me.TableLayoutPanel12.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel12.Size = New System.Drawing.Size(388, 46)
        Me.TableLayoutPanel12.TabIndex = 1
        '
        'Label14
        '
        Me.Label14.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(3, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(91, 46)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "Période du"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label15
        '
        Me.Label15.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(197, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(94, 46)
        Me.Label15.TabIndex = 1
        Me.Label15.Text = "au"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dtp_PériodeDébut35
        '
        Me.dtp_PériodeDébut35.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtp_PériodeDébut35.Enabled = False
        Me.dtp_PériodeDébut35.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_PériodeDébut35.Location = New System.Drawing.Point(100, 12)
        Me.dtp_PériodeDébut35.Margin = New System.Windows.Forms.Padding(3, 12, 3, 3)
        Me.dtp_PériodeDébut35.Name = "dtp_PériodeDébut35"
        Me.dtp_PériodeDébut35.Size = New System.Drawing.Size(91, 20)
        Me.dtp_PériodeDébut35.TabIndex = 2
        '
        'dtp_PériodeFin35
        '
        Me.dtp_PériodeFin35.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtp_PériodeFin35.Enabled = False
        Me.dtp_PériodeFin35.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_PériodeFin35.Location = New System.Drawing.Point(297, 12)
        Me.dtp_PériodeFin35.Margin = New System.Windows.Forms.Padding(3, 12, 3, 3)
        Me.dtp_PériodeFin35.Name = "dtp_PériodeFin35"
        Me.dtp_PériodeFin35.Size = New System.Drawing.Size(88, 20)
        Me.dtp_PériodeFin35.TabIndex = 3
        '
        'TableLayoutPanel9
        '
        Me.TableLayoutPanel9.ColumnCount = 1
        Me.TableLayoutPanel9.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel9.Controls.Add(Me.cbx_Objet45, 0, 0)
        Me.TableLayoutPanel9.Controls.Add(Me.TableLayoutPanel10, 0, 1)
        Me.TableLayoutPanel9.Location = New System.Drawing.Point(105, 485)
        Me.TableLayoutPanel9.Name = "TableLayoutPanel9"
        Me.TableLayoutPanel9.RowCount = 2
        Me.TableLayoutPanel9.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 36.36364!))
        Me.TableLayoutPanel9.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 63.63636!))
        Me.TableLayoutPanel9.Size = New System.Drawing.Size(394, 77)
        Me.TableLayoutPanel9.TabIndex = 46
        '
        'cbx_Objet45
        '
        Me.cbx_Objet45.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbx_Objet45.FormattingEnabled = True
        Me.cbx_Objet45.Items.AddRange(New Object() {"AIDE A LA PERSONNE", "MENAGE", "AUTRE"})
        Me.cbx_Objet45.Location = New System.Drawing.Point(3, 3)
        Me.cbx_Objet45.Name = "cbx_Objet45"
        Me.cbx_Objet45.Size = New System.Drawing.Size(196, 21)
        Me.cbx_Objet45.TabIndex = 0
        '
        'TableLayoutPanel10
        '
        Me.TableLayoutPanel10.ColumnCount = 4
        Me.TableLayoutPanel10.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel10.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel10.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TableLayoutPanel10.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 94.0!))
        Me.TableLayoutPanel10.Controls.Add(Me.Label12, 0, 0)
        Me.TableLayoutPanel10.Controls.Add(Me.Label13, 2, 0)
        Me.TableLayoutPanel10.Controls.Add(Me.dtp_PériodeDébut45, 1, 0)
        Me.TableLayoutPanel10.Controls.Add(Me.dtp_PériodeFin45, 3, 0)
        Me.TableLayoutPanel10.Location = New System.Drawing.Point(3, 31)
        Me.TableLayoutPanel10.Name = "TableLayoutPanel10"
        Me.TableLayoutPanel10.RowCount = 1
        Me.TableLayoutPanel10.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel10.Size = New System.Drawing.Size(388, 43)
        Me.TableLayoutPanel10.TabIndex = 1
        '
        'Label12
        '
        Me.Label12.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(3, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(91, 43)
        Me.Label12.TabIndex = 0
        Me.Label12.Text = "Période du"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label13
        '
        Me.Label13.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(197, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(94, 43)
        Me.Label13.TabIndex = 1
        Me.Label13.Text = "au"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dtp_PériodeDébut45
        '
        Me.dtp_PériodeDébut45.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtp_PériodeDébut45.Enabled = False
        Me.dtp_PériodeDébut45.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_PériodeDébut45.Location = New System.Drawing.Point(100, 12)
        Me.dtp_PériodeDébut45.Margin = New System.Windows.Forms.Padding(3, 12, 3, 3)
        Me.dtp_PériodeDébut45.Name = "dtp_PériodeDébut45"
        Me.dtp_PériodeDébut45.Size = New System.Drawing.Size(91, 20)
        Me.dtp_PériodeDébut45.TabIndex = 2
        '
        'dtp_PériodeFin45
        '
        Me.dtp_PériodeFin45.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtp_PériodeFin45.Enabled = False
        Me.dtp_PériodeFin45.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_PériodeFin45.Location = New System.Drawing.Point(297, 12)
        Me.dtp_PériodeFin45.Margin = New System.Windows.Forms.Padding(3, 12, 3, 3)
        Me.dtp_PériodeFin45.Name = "dtp_PériodeFin45"
        Me.dtp_PériodeFin45.Size = New System.Drawing.Size(88, 20)
        Me.dtp_PériodeFin45.TabIndex = 3
        '
        'btn_Génération5
        '
        Me.btn_Génération5.AutoSize = True
        Me.btn_Génération5.BackgroundImage = CType(resources.GetObject("btn_Génération5.BackgroundImage"), System.Drawing.Image)
        Me.btn_Génération5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_Génération5.Enabled = False
        Me.btn_Génération5.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Génération5.ForeColor = System.Drawing.Color.White
        Me.btn_Génération5.Location = New System.Drawing.Point(321, 615)
        Me.btn_Génération5.Name = "btn_Génération5"
        Me.btn_Génération5.Size = New System.Drawing.Size(180, 37)
        Me.btn_Génération5.TabIndex = 2
        Me.btn_Génération5.Text = "Générer la facture"
        Me.btn_Génération5.UseVisualStyleBackColor = True
        '
        'btn_Calculer5
        '
        Me.btn_Calculer5.AutoSize = True
        Me.btn_Calculer5.BackgroundImage = CType(resources.GetObject("btn_Calculer5.BackgroundImage"), System.Drawing.Image)
        Me.btn_Calculer5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_Calculer5.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Calculer5.ForeColor = System.Drawing.Color.White
        Me.btn_Calculer5.Location = New System.Drawing.Point(507, 615)
        Me.btn_Calculer5.Name = "btn_Calculer5"
        Me.btn_Calculer5.Size = New System.Drawing.Size(180, 37)
        Me.btn_Calculer5.TabIndex = 3
        Me.btn_Calculer5.Text = "Calculer"
        Me.btn_Calculer5.UseVisualStyleBackColor = True
        '
        'win_CréationFacture
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1015, 661)
        Me.Controls.Add(Me.btn_Calculer5)
        Me.Controls.Add(Me.btn_Génération5)
        Me.Controls.Add(Me.tlp_Facture5)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "win_CréationFacture"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Système de facturation - Création d'une facture"
        Me.tlp_Facture5.ResumeLayout(False)
        Me.tlp_Facture5.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel3.PerformLayout()
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.TableLayoutPanel4.PerformLayout()
        Me.TableLayoutPanel5.ResumeLayout(False)
        Me.TableLayoutPanel6.ResumeLayout(False)
        Me.TableLayoutPanel6.PerformLayout()
        Me.TableLayoutPanel7.ResumeLayout(False)
        Me.TableLayoutPanel8.ResumeLayout(False)
        Me.TableLayoutPanel8.PerformLayout()
        Me.TableLayoutPanel11.ResumeLayout(False)
        Me.TableLayoutPanel12.ResumeLayout(False)
        Me.TableLayoutPanel12.PerformLayout()
        Me.TableLayoutPanel9.ResumeLayout(False)
        Me.TableLayoutPanel10.ResumeLayout(False)
        Me.TableLayoutPanel10.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents tlp_Facture5 As TableLayoutPanel
    Friend WithEvents btn_Génération5 As Button
    Friend WithEvents lbl_ÉchéanceFacture5 As Label
    Friend WithEvents lbl_ModeRèglementFacture5 As Label
    Friend WithEvents lbl_IdentitéClientFacture5 As Label
    Friend WithEvents lbl_Facture5 As Label
    Friend WithEvents lbl_Type5 As Label
    Friend WithEvents lbl_Référence5 As Label
    Friend WithEvents lbl_MoisFacture5 As Label
    Friend WithEvents lbl_NuméroFacture5 As Label
    Friend WithEvents lbl_TypeClientFacture5 As Label
    Friend WithEvents lbl_NumClientFacture5 As Label
    Friend WithEvents lbl_DateFacture5 As Label
    Friend WithEvents dtp_Date5 As DateTimePicker
    Friend WithEvents cbx_Mois5 As ComboBox
    Friend WithEvents lbl_NomPrénom5 As Label
    Friend WithEvents lbl_Échéance5 As Label
    Friend WithEvents lbl_PrixTTCFacture5 As Label
    Friend WithEvents lbl_QuantitéHeureFacture5 As Label
    Friend WithEvents lbl_PrixUnitaireFacture5 As Label
    Friend WithEvents lbl_DésignationFacture5 As Label
    Friend WithEvents lbl_CodeFacture5 As Label
    Friend WithEvents cbx_Règlement5 As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents lbl_TotalTTC5 As Label
    Friend WithEvents lbl_Prix15 As Label
    Friend WithEvents lbl_Prix25 As Label
    Friend WithEvents lbl_Prix35 As Label
    Friend WithEvents lbl_Prix45 As Label
    Friend WithEvents txb_QuantitéHeure15 As TextBox
    Friend WithEvents txb_QuantitéHeure25 As TextBox
    Friend WithEvents txb_QuantitéHeure35 As TextBox
    Friend WithEvents txb_QuantitéHeure45 As TextBox
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents txb_PrixUnitaire15 As TextBox
    Friend WithEvents cbx_Majoration15 As ComboBox
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents txb_PrixUnitaire25 As TextBox
    Friend WithEvents cbx_Majoration25 As ComboBox
    Friend WithEvents TableLayoutPanel3 As TableLayoutPanel
    Friend WithEvents txb_PrixUnitaire35 As TextBox
    Friend WithEvents cbx_Majoration35 As ComboBox
    Friend WithEvents TableLayoutPanel4 As TableLayoutPanel
    Friend WithEvents txb_PrixUnitaire45 As TextBox
    Friend WithEvents cbx_Majoration45 As ComboBox
    Friend WithEvents TableLayoutPanel5 As TableLayoutPanel
    Friend WithEvents cbx_Objet15 As ComboBox
    Friend WithEvents TableLayoutPanel6 As TableLayoutPanel
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents dtp_PériodeDébut15 As DateTimePicker
    Friend WithEvents dtp_PériodeFin15 As DateTimePicker
    Friend WithEvents TableLayoutPanel7 As TableLayoutPanel
    Friend WithEvents cbx_Objet25 As ComboBox
    Friend WithEvents TableLayoutPanel8 As TableLayoutPanel
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents dtp_PériodeDébut25 As DateTimePicker
    Friend WithEvents dtp_PériodeFin25 As DateTimePicker
    Friend WithEvents TableLayoutPanel11 As TableLayoutPanel
    Friend WithEvents cbx_Objet35 As ComboBox
    Friend WithEvents TableLayoutPanel12 As TableLayoutPanel
    Friend WithEvents Label14 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents dtp_PériodeDébut35 As DateTimePicker
    Friend WithEvents dtp_PériodeFin35 As DateTimePicker
    Friend WithEvents TableLayoutPanel9 As TableLayoutPanel
    Friend WithEvents cbx_Objet45 As ComboBox
    Friend WithEvents TableLayoutPanel10 As TableLayoutPanel
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents dtp_PériodeDébut45 As DateTimePicker
    Friend WithEvents dtp_PériodeFin45 As DateTimePicker
    Friend WithEvents btn_Calculer5 As Button
End Class

﻿Public Class win_ChoixClient
    Private Sub win_ChoixClient_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ConnexionBDD("SELECT num_client, CONCAT(nom,' ', prenom) AS name FROM client WHERE suppr=0", "ListeClients")

        cbx_ListeClients4.DataSource = DonnéesListe
        cbx_ListeClients4.DisplayMember = "name"
        cbx_ListeClients4.ValueMember = "num_client"
    End Sub

    Private Sub btn_ChoixClient4_Click(sender As Object, e As EventArgs) Handles btn_ChoixClient4.Click
        DonnéesBDD.Reset()
        ConnexionBDD("SELECT * FROM client WHERE suppr=0")
        DonnéesChoixClient = DonnéesBDD.Select("num_client='" & cbx_ListeClients4.SelectedValue.ToString() & "'")

        If cbx_TypeFacture.SelectedItem = "Forfait" Then
            win_CréationForfait.Show()
            win_Accueil.Hide()
            Me.Close()
        Else
            win_CréationFacture.Show()
            win_Accueil.Hide()
            Me.Close()
        End If

    End Sub
End Class
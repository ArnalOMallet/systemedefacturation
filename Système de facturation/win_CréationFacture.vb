﻿Public Class win_CréationFacture
    Private Sub win_CréationFacture_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        win_Accueil.Show()
    End Sub

    Private Sub cbx_Objet15_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbx_Objet15.SelectedIndexChanged
        txb_PrixUnitaire15.Enabled = True
        txb_QuantitéHeure15.Enabled = True
        dtp_PériodeDébut15.Enabled = True
        dtp_PériodeFin15.Enabled = True
    End Sub

    Private Sub cbx_Objet25_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbx_Objet25.SelectedIndexChanged
        txb_PrixUnitaire25.Enabled = True
        txb_QuantitéHeure25.Enabled = True
        dtp_PériodeDébut25.Enabled = True
        dtp_PériodeFin25.Enabled = True
    End Sub

    Private Sub cbx_Objet35_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbx_Objet35.SelectedIndexChanged
        txb_PrixUnitaire35.Enabled = True
        txb_QuantitéHeure35.Enabled = True
        dtp_PériodeDébut35.Enabled = True
        dtp_PériodeFin35.Enabled = True
    End Sub

    Private Sub cbx_Objet45_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbx_Objet45.SelectedIndexChanged
        txb_PrixUnitaire45.Enabled = True
        txb_QuantitéHeure45.Enabled = True
        dtp_PériodeDébut45.Enabled = True
        dtp_PériodeFin45.Enabled = True
    End Sub

    Private Sub win_CréationFacture_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ConnexionBDD("SELECT * FROM facture", "NuméroFacture")
        Dim ParticuleSexe As String = "Mme "
        If DonnéesChoixClient(0).ItemArray(1) = "h" Then
            ParticuleSexe = "Mr "
        End If
        lbl_Type5.Text = DonnéesChoixClient(0).ItemArray(7)
        If DonnéesChoixClient(0).ItemArray(7) = "Professionel" Then
            lbl_NomPrénom5.Text = ParticuleSexe & DonnéesChoixClient(0).ItemArray(2) & " " & DonnéesChoixClient(0).ItemArray(3) & " - " & DonnéesChoixClient(0).ItemArray(4)
        Else
            lbl_NomPrénom5.Text = ParticuleSexe & DonnéesChoixClient(0).ItemArray(2) & " " & DonnéesChoixClient(0).ItemArray(3)
        End If

        lbl_Référence5.Text = DonnéesChoixClient(0).ItemArray(8)
        lbl_Facture5.Text = NuméroFacture.Rows(0).Item(1)

    End Sub

    Private Sub btn_Génération5_Click(sender As Object, e As EventArgs) Handles btn_Génération5.Click
        DonnéesFacture(0) = dtp_Date5.Value
        DonnéesFacture(1) = lbl_Référence5.Text
        DonnéesFacture(2) = lbl_Type5.Text
        DonnéesFacture(3) = lbl_Facture5.Text
        DonnéesFacture(4) = cbx_Mois5.SelectedItem
        DonnéesFacture(5) = lbl_NomPrénom5.Text
        DonnéesFacture(6) = cbx_Règlement5.SelectedItem

        If DonnéesChoixClient(0).ItemArray(7) = "Particulier" Then
            DonnéesFacture(7) = DonnéesChoixClient(0).ItemArray(2) + DonnéesChoixClient(0).ItemArray(3)
        Else
            DonnéesFacture(7) = DonnéesChoixClient(0).ItemArray(4)
        End If
        DonnéesFacture(8) = DonnéesChoixClient(0).ItemArray(5)
        DonnéesFacture(9) = DonnéesChoixClient(0).ItemArray(6)

        DonnéesFacture(10) = lbl_TotalTTC5.Text


        If cbx_Objet15.SelectedIndex > -1 Then
            DonnéesFacture(11) = cbx_Objet15.SelectedItem
            DonnéesFacture(12) = dtp_PériodeDébut15.Value.ToString
            DonnéesFacture(13) = dtp_PériodeFin15.Value.ToString
            DonnéesFacture(14) = txb_PrixUnitaire15.Text
            DonnéesFacture(15) = txb_QuantitéHeure15.Text
            DonnéesFacture(16) = lbl_Prix15.Text
            DonnéesFacture(35) = 1
        End If

        If cbx_Objet25.SelectedIndex > -1 Then
            DonnéesFacture(17) = cbx_Objet25.SelectedItem
            DonnéesFacture(18) = dtp_PériodeDébut25.Value.ToString
            DonnéesFacture(19) = dtp_PériodeFin25.Value.ToString
            DonnéesFacture(20) = txb_PrixUnitaire25.Text
            DonnéesFacture(21) = txb_QuantitéHeure25.Text
            DonnéesFacture(22) = lbl_Prix25.Text
            DonnéesFacture(35) = 2
        End If

        If cbx_Objet35.SelectedIndex > -1 Then
            DonnéesFacture(23) = cbx_Objet35.SelectedItem
            DonnéesFacture(24) = dtp_PériodeDébut35.Value.ToString
            DonnéesFacture(25) = dtp_PériodeFin35.Value.ToString
            DonnéesFacture(26) = txb_PrixUnitaire35.Text
            DonnéesFacture(27) = txb_QuantitéHeure35.Text
            DonnéesFacture(28) = lbl_Prix35.Text
            DonnéesFacture(35) = 3
        End If

        If cbx_Objet45.SelectedIndex > -1 Then
            DonnéesFacture(29) = cbx_Objet45.SelectedItem
            DonnéesFacture(30) = dtp_PériodeDébut45.Value.ToString
            DonnéesFacture(31) = dtp_PériodeFin45.Value.ToString
            DonnéesFacture(32) = txb_PrixUnitaire45.Text
            DonnéesFacture(33) = txb_QuantitéHeure45.Text
            DonnéesFacture(34) = lbl_Prix45.Text
            DonnéesFacture(35) = 4
        End If

        GénérationPDF_Normale(DonnéesFacture)

    End Sub

    Private Sub btn_Calculer5_Click(sender As Object, e As EventArgs) Handles btn_Calculer5.Click
        Dim TotalTTC As Single = 0
        Dim PU15 As Single = 0
        Dim PU25 As Single = 0
        Dim PU35 As Single = 0
        Dim PU45 As Single = 0
        Dim QH15 As Single = 0
        Dim QH25 As Single = 0
        Dim QH35 As Single = 0
        Dim QH45 As Single = 0


        If Not String.IsNullOrWhiteSpace(cbx_Objet15.SelectedItem) Then
            txb_PrixUnitaire15.Text = txb_PrixUnitaire15.Text.Replace(".", ",")
            txb_QuantitéHeure15.Text = txb_QuantitéHeure15.Text.Replace(".", ",")
            PU15 = txb_PrixUnitaire15.Text
            QH15 = txb_QuantitéHeure15.Text

            lbl_Prix15.Text = PU15 * QH15
            TotalTTC = TotalTTC + lbl_Prix15.Text
        End If
        If Not String.IsNullOrWhiteSpace(cbx_Objet25.SelectedItem) Then
            txb_PrixUnitaire25.Text = txb_PrixUnitaire25.Text.Replace(".", ",")
            txb_QuantitéHeure25.Text = txb_QuantitéHeure25.Text.Replace(".", ",")
            PU25 = txb_PrixUnitaire25.Text
            QH25 = txb_QuantitéHeure25.Text

            lbl_Prix25.Text = PU25 * QH25
            TotalTTC = TotalTTC + lbl_Prix25.Text
        End If
        If Not String.IsNullOrWhiteSpace(cbx_Objet35.SelectedItem) Then
            txb_PrixUnitaire35.Text = txb_PrixUnitaire35.Text.Replace(".", ",")
            txb_QuantitéHeure35.Text = txb_QuantitéHeure35.Text.Replace(".", ",")
            PU35 = txb_PrixUnitaire35.Text
            QH35 = txb_QuantitéHeure35.Text

            lbl_Prix35.Text = PU35 * QH35
            TotalTTC = TotalTTC + lbl_Prix35.Text
        End If
        If Not String.IsNullOrWhiteSpace(cbx_Objet45.SelectedItem) Then
            txb_PrixUnitaire45.Text = txb_PrixUnitaire45.Text.Replace(".", ",")
            txb_QuantitéHeure45.Text = txb_QuantitéHeure45.Text.Replace(".", ",")
            PU45 = txb_PrixUnitaire45.Text
            QH45 = txb_QuantitéHeure45.Text

            lbl_Prix45.Text = PU45 * QH45
            TotalTTC = TotalTTC + lbl_Prix45.Text
        End If
        lbl_TotalTTC5.Text = TotalTTC

        btn_Génération5.Enabled = True
    End Sub

    Private Sub txb_PrixUnitaire15_TextChanged(sender As Object, e As EventArgs) Handles txb_PrixUnitaire15.TextChanged
        If txb_PrixUnitaire15.Text.Length > 0 Then
            cbx_Majoration15.Enabled = True
        Else
            cbx_Majoration15.Enabled = False
        End If
    End Sub
    Private Sub txb_PrixUnitaire25_TextChanged(sender As Object, e As EventArgs) Handles txb_PrixUnitaire25.TextChanged
        If txb_PrixUnitaire25.Text.Length > 0 Then
            cbx_Majoration25.Enabled = True
        Else
            cbx_Majoration25.Enabled = False
        End If
    End Sub
    Private Sub txb_PrixUnitaire35_TextChanged(sender As Object, e As EventArgs) Handles txb_PrixUnitaire35.TextChanged
        If txb_PrixUnitaire35.Text.Length > 0 Then
            cbx_Majoration35.Enabled = True
        Else
            cbx_Majoration35.Enabled = False
        End If
    End Sub
    Private Sub txb_PrixUnitaire45_TextChanged(sender As Object, e As EventArgs) Handles txb_PrixUnitaire45.TextChanged
        If txb_PrixUnitaire45.Text.Length > 0 Then
            cbx_Majoration45.Enabled = True
        Else
            cbx_Majoration45.Enabled = False
        End If
    End Sub

    Private Sub cbx_Majoration15_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbx_Majoration15.SelectedIndexChanged
        If PrixUnitaireOrigine(0) = 0 Then
            PrixUnitaireOrigine(0) = txb_PrixUnitaire15.Text
        End If

        If String.IsNullOrWhiteSpace(cbx_Majoration15.SelectedItem) Then
            txb_PrixUnitaire15.Text = PrixUnitaireOrigine(0)
        ElseIf cbx_Majoration15.SelectedItem = "Majoration 5%" Then
            txb_PrixUnitaire15.Text = PrixUnitaireOrigine(0) + (PrixUnitaireOrigine(0) * 0.05)
        ElseIf cbx_Majoration15.SelectedItem = "Majoration 10%" Then
            txb_PrixUnitaire15.Text = PrixUnitaireOrigine(0) + (PrixUnitaireOrigine(0) * 0.1)
        ElseIf cbx_Majoration15.SelectedItem = "Majoration 25%" Then
            txb_PrixUnitaire15.Text = PrixUnitaireOrigine(0) + (PrixUnitaireOrigine(0) * 0.25)
        End If
    End Sub

    Private Sub cbx_Majoration25_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbx_Majoration25.SelectedIndexChanged
        If PrixUnitaireOrigine(1) = 0 Then
            PrixUnitaireOrigine(1) = txb_PrixUnitaire25.Text
        End If

        If String.IsNullOrWhiteSpace(cbx_Majoration25.SelectedItem) Then
            txb_PrixUnitaire25.Text = PrixUnitaireOrigine(1)
        ElseIf cbx_Majoration25.SelectedItem = "Majoration 5%" Then
            txb_PrixUnitaire25.Text = PrixUnitaireOrigine(1) + (PrixUnitaireOrigine(1) * 0.05)
        ElseIf cbx_Majoration25.SelectedItem = "Majoration 10%" Then
            txb_PrixUnitaire25.Text = PrixUnitaireOrigine(1) + (PrixUnitaireOrigine(1) * 0.1)
        ElseIf cbx_Majoration25.SelectedItem = "Majoration 25%" Then
            txb_PrixUnitaire25.Text = PrixUnitaireOrigine(1) + (PrixUnitaireOrigine(1) * 0.25)
        End If
    End Sub

    Private Sub cbx_Majoration35_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbx_Majoration35.SelectedIndexChanged
        If PrixUnitaireOrigine(2) = 0 Then
            PrixUnitaireOrigine(2) = txb_PrixUnitaire35.Text
        End If

        If String.IsNullOrWhiteSpace(cbx_Majoration35.SelectedItem) Then
            txb_PrixUnitaire35.Text = PrixUnitaireOrigine(2)
        ElseIf cbx_Majoration35.SelectedItem = "Majoration 5%" Then
            txb_PrixUnitaire35.Text = PrixUnitaireOrigine(2) + (PrixUnitaireOrigine(2) * 0.05)
        ElseIf cbx_Majoration35.SelectedItem = "Majoration 10%" Then
            txb_PrixUnitaire35.Text = PrixUnitaireOrigine(2) + (PrixUnitaireOrigine(2) * 0.1)
        ElseIf cbx_Majoration35.SelectedItem = "Majoration 25%" Then
            txb_PrixUnitaire35.Text = PrixUnitaireOrigine(2) + (PrixUnitaireOrigine(2) * 0.25)
        End If
    End Sub

    Private Sub cbx_Majoration45_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbx_Majoration45.SelectedIndexChanged
        If PrixUnitaireOrigine(3) = 0 Then
            PrixUnitaireOrigine(3) = txb_PrixUnitaire45.Text
        End If

        If String.IsNullOrWhiteSpace(cbx_Majoration45.SelectedItem) Then
            txb_PrixUnitaire45.Text = PrixUnitaireOrigine(3)
        ElseIf cbx_Majoration45.SelectedItem = "Majoration 5%" Then
            txb_PrixUnitaire45.Text = PrixUnitaireOrigine(3) + (PrixUnitaireOrigine(3) * 0.05)
        ElseIf cbx_Majoration45.SelectedItem = "Majoration 10%" Then
            txb_PrixUnitaire45.Text = PrixUnitaireOrigine(3) + (PrixUnitaireOrigine(3) * 0.1)
        ElseIf cbx_Majoration45.SelectedItem = "Majoration 25%" Then
            txb_PrixUnitaire45.Text = PrixUnitaireOrigine(3) + (PrixUnitaireOrigine(3) * 0.25)
        End If
    End Sub
End Class
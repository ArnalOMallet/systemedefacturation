﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class win_Accueil
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(win_Accueil))
        Me.btn_GestionClient1 = New System.Windows.Forms.Button()
        Me.btn_CréationFacture1 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btn_GestionClient1
        '
        Me.btn_GestionClient1.AutoSize = True
        Me.btn_GestionClient1.BackgroundImage = CType(resources.GetObject("btn_GestionClient1.BackgroundImage"), System.Drawing.Image)
        Me.btn_GestionClient1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_GestionClient1.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_GestionClient1.ForeColor = System.Drawing.Color.White
        Me.btn_GestionClient1.Location = New System.Drawing.Point(288, 181)
        Me.btn_GestionClient1.Name = "btn_GestionClient1"
        Me.btn_GestionClient1.Size = New System.Drawing.Size(180, 37)
        Me.btn_GestionClient1.TabIndex = 0
        Me.btn_GestionClient1.Text = "Gestion des clients"
        Me.btn_GestionClient1.UseVisualStyleBackColor = True
        '
        'btn_CréationFacture1
        '
        Me.btn_CréationFacture1.AutoSize = True
        Me.btn_CréationFacture1.BackgroundImage = CType(resources.GetObject("btn_CréationFacture1.BackgroundImage"), System.Drawing.Image)
        Me.btn_CréationFacture1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_CréationFacture1.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_CréationFacture1.ForeColor = System.Drawing.Color.White
        Me.btn_CréationFacture1.Location = New System.Drawing.Point(505, 181)
        Me.btn_CréationFacture1.Name = "btn_CréationFacture1"
        Me.btn_CréationFacture1.Size = New System.Drawing.Size(180, 37)
        Me.btn_CréationFacture1.TabIndex = 1
        Me.btn_CréationFacture1.Text = "Création d'une facture"
        Me.btn_CréationFacture1.UseVisualStyleBackColor = True
        '
        'win_Accueil
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(984, 461)
        Me.Controls.Add(Me.btn_CréationFacture1)
        Me.Controls.Add(Me.btn_GestionClient1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "win_Accueil"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Système de facturation - Accueil"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btn_GestionClient1 As Button
    Friend WithEvents btn_CréationFacture1 As Button
End Class

﻿Public Class win_CréationForfait
    Private Sub txb_PrixUnitaire15_TextChanged(sender As Object, e As EventArgs) Handles txb_PrixUnitaire15.TextChanged
        lbl_Prix15.Text = txb_PrixUnitaire15.Text
        lbl_TotalTTC5.Text = txb_PrixUnitaire15.Text

    End Sub

    Private Sub cbx_Objet15_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbx_Objet15.SelectedIndexChanged
        If cbx_Objet15.SelectedItem <> "" Then
            txb_PrixUnitaire15.Enabled = True
        End If
    End Sub

    Private Sub win_CréationForfait_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ConnexionBDD("SELECT * FROM facture", "NuméroFacture")
        Dim ParticuleSexe As String = "Mme "
        If DonnéesChoixClient(0).ItemArray(1) = "h" Then
            ParticuleSexe = "Mr "
        End If
        lbl_Type5.Text = DonnéesChoixClient(0).ItemArray(7)
        If DonnéesChoixClient(0).ItemArray(7) = "Professionel" Then
            lbl_NomPrénom5.Text = ParticuleSexe & DonnéesChoixClient(0).ItemArray(2) & " " & DonnéesChoixClient(0).ItemArray(3) & " - " & DonnéesChoixClient(0).ItemArray(4)
        Else
            lbl_NomPrénom5.Text = ParticuleSexe & DonnéesChoixClient(0).ItemArray(2) & " " & DonnéesChoixClient(0).ItemArray(3)
        End If

        lbl_Référence5.Text = DonnéesChoixClient(0).ItemArray(8)
        lbl_Facture5.Text = NuméroFacture.Rows(0).Item(1)

        btn_Génération5.Enabled = True
    End Sub

    Private Sub btn_Génération5_Click(sender As Object, e As EventArgs) Handles btn_Génération5.Click
        DonnéesFacture(0) = dtp_Date5.Value
        DonnéesFacture(1) = lbl_Référence5.Text
        DonnéesFacture(2) = lbl_Type5.Text
        DonnéesFacture(3) = lbl_Facture5.Text
        DonnéesFacture(4) = cbx_Mois5.SelectedItem
        DonnéesFacture(5) = lbl_NomPrénom5.Text
        DonnéesFacture(6) = cbx_Règlement5.SelectedItem
        DonnéesFacture(7) = cbx_Objet15.SelectedItem
        DonnéesFacture(8) = lbl_Prix15.Text
        DonnéesFacture(9) = lbl_TotalTTC5.Text
        If DonnéesChoixClient(0).ItemArray(7) = "Particulier" Then
            DonnéesFacture(10) = DonnéesChoixClient(0).ItemArray(2) + DonnéesChoixClient(0).ItemArray(3)
        Else
            DonnéesFacture(10) = DonnéesChoixClient(0).ItemArray(4)
        End If
        DonnéesFacture(11) = DonnéesChoixClient(0).ItemArray(5)
        DonnéesFacture(12) = DonnéesChoixClient(0).ItemArray(6)

        GénérationPDF_Forfait(DonnéesFacture)

    End Sub
End Class
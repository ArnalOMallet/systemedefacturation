﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class win_CréationForfait
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(win_CréationForfait))
        Me.btn_Génération5 = New System.Windows.Forms.Button()
        Me.lbl_PrixTTCFacture5 = New System.Windows.Forms.Label()
        Me.lbl_QuantitéHeureFacture5 = New System.Windows.Forms.Label()
        Me.lbl_PrixUnitaireFacture5 = New System.Windows.Forms.Label()
        Me.lbl_DésignationFacture5 = New System.Windows.Forms.Label()
        Me.lbl_CodeFacture5 = New System.Windows.Forms.Label()
        Me.lbl_ÉchéanceFacture5 = New System.Windows.Forms.Label()
        Me.lbl_ModeRèglementFacture5 = New System.Windows.Forms.Label()
        Me.lbl_IdentitéClientFacture5 = New System.Windows.Forms.Label()
        Me.lbl_Facture5 = New System.Windows.Forms.Label()
        Me.lbl_Type5 = New System.Windows.Forms.Label()
        Me.lbl_Référence5 = New System.Windows.Forms.Label()
        Me.lbl_MoisFacture5 = New System.Windows.Forms.Label()
        Me.lbl_NuméroFacture5 = New System.Windows.Forms.Label()
        Me.lbl_TypeClientFacture5 = New System.Windows.Forms.Label()
        Me.lbl_NumClientFacture5 = New System.Windows.Forms.Label()
        Me.lbl_DateFacture5 = New System.Windows.Forms.Label()
        Me.dtp_Date5 = New System.Windows.Forms.DateTimePicker()
        Me.tlp_Facture5 = New System.Windows.Forms.TableLayoutPanel()
        Me.txb_PrixUnitaire15 = New System.Windows.Forms.TextBox()
        Me.cbx_Mois5 = New System.Windows.Forms.ComboBox()
        Me.lbl_NomPrénom5 = New System.Windows.Forms.Label()
        Me.lbl_Échéance5 = New System.Windows.Forms.Label()
        Me.cbx_Règlement5 = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lbl_Prix15 = New System.Windows.Forms.Label()
        Me.cbx_Objet15 = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lbl_TotalTTC5 = New System.Windows.Forms.Label()
        Me.tlp_Facture5.SuspendLayout()
        Me.SuspendLayout()
        '
        'btn_Génération5
        '
        Me.btn_Génération5.AutoSize = True
        Me.btn_Génération5.BackgroundImage = CType(resources.GetObject("btn_Génération5.BackgroundImage"), System.Drawing.Image)
        Me.btn_Génération5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_Génération5.Enabled = False
        Me.btn_Génération5.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Génération5.ForeColor = System.Drawing.Color.White
        Me.btn_Génération5.Location = New System.Drawing.Point(432, 618)
        Me.btn_Génération5.Name = "btn_Génération5"
        Me.btn_Génération5.Size = New System.Drawing.Size(180, 37)
        Me.btn_Génération5.TabIndex = 5
        Me.btn_Génération5.Text = "Générer la facture"
        Me.btn_Génération5.UseVisualStyleBackColor = True
        '
        'lbl_PrixTTCFacture5
        '
        Me.lbl_PrixTTCFacture5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_PrixTTCFacture5.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_PrixTTCFacture5.Location = New System.Drawing.Point(917, 137)
        Me.lbl_PrixTTCFacture5.Name = "lbl_PrixTTCFacture5"
        Me.lbl_PrixTTCFacture5.Size = New System.Drawing.Size(93, 40)
        Me.lbl_PrixTTCFacture5.TabIndex = 27
        Me.lbl_PrixTTCFacture5.Text = "Prix TTC"
        Me.lbl_PrixTTCFacture5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_QuantitéHeureFacture5
        '
        Me.lbl_QuantitéHeureFacture5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_QuantitéHeureFacture5.AutoSize = True
        Me.lbl_QuantitéHeureFacture5.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_QuantitéHeureFacture5.Location = New System.Drawing.Point(707, 137)
        Me.lbl_QuantitéHeureFacture5.Name = "lbl_QuantitéHeureFacture5"
        Me.lbl_QuantitéHeureFacture5.Size = New System.Drawing.Size(194, 40)
        Me.lbl_QuantitéHeureFacture5.TabIndex = 26
        Me.lbl_QuantitéHeureFacture5.Text = "Quantité en heure"
        Me.lbl_QuantitéHeureFacture5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_PrixUnitaireFacture5
        '
        Me.lbl_PrixUnitaireFacture5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_PrixUnitaireFacture5.AutoSize = True
        Me.lbl_PrixUnitaireFacture5.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_PrixUnitaireFacture5.Location = New System.Drawing.Point(506, 137)
        Me.lbl_PrixUnitaireFacture5.Name = "lbl_PrixUnitaireFacture5"
        Me.lbl_PrixUnitaireFacture5.Size = New System.Drawing.Size(194, 40)
        Me.lbl_PrixUnitaireFacture5.TabIndex = 25
        Me.lbl_PrixUnitaireFacture5.Text = "Prix unitaire TTC"
        Me.lbl_PrixUnitaireFacture5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_DésignationFacture5
        '
        Me.lbl_DésignationFacture5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_DésignationFacture5.AutoSize = True
        Me.lbl_DésignationFacture5.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_DésignationFacture5.Location = New System.Drawing.Point(105, 137)
        Me.lbl_DésignationFacture5.Name = "lbl_DésignationFacture5"
        Me.lbl_DésignationFacture5.Size = New System.Drawing.Size(394, 40)
        Me.lbl_DésignationFacture5.TabIndex = 24
        Me.lbl_DésignationFacture5.Text = "Désignation"
        Me.lbl_DésignationFacture5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_CodeFacture5
        '
        Me.lbl_CodeFacture5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_CodeFacture5.AutoSize = True
        Me.lbl_CodeFacture5.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_CodeFacture5.Location = New System.Drawing.Point(4, 137)
        Me.lbl_CodeFacture5.Name = "lbl_CodeFacture5"
        Me.lbl_CodeFacture5.Size = New System.Drawing.Size(94, 40)
        Me.lbl_CodeFacture5.TabIndex = 23
        Me.lbl_CodeFacture5.Text = "Code"
        Me.lbl_CodeFacture5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_ÉchéanceFacture5
        '
        Me.lbl_ÉchéanceFacture5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_ÉchéanceFacture5.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ÉchéanceFacture5.Location = New System.Drawing.Point(917, 69)
        Me.lbl_ÉchéanceFacture5.Name = "lbl_ÉchéanceFacture5"
        Me.lbl_ÉchéanceFacture5.Size = New System.Drawing.Size(93, 40)
        Me.lbl_ÉchéanceFacture5.TabIndex = 18
        Me.lbl_ÉchéanceFacture5.Text = "Échéance"
        Me.lbl_ÉchéanceFacture5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_ModeRèglementFacture5
        '
        Me.lbl_ModeRèglementFacture5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_ModeRèglementFacture5.AutoSize = True
        Me.tlp_Facture5.SetColumnSpan(Me.lbl_ModeRèglementFacture5, 2)
        Me.lbl_ModeRèglementFacture5.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ModeRèglementFacture5.Location = New System.Drawing.Point(506, 69)
        Me.lbl_ModeRèglementFacture5.Name = "lbl_ModeRèglementFacture5"
        Me.lbl_ModeRèglementFacture5.Size = New System.Drawing.Size(395, 40)
        Me.lbl_ModeRèglementFacture5.TabIndex = 16
        Me.lbl_ModeRèglementFacture5.Text = "Mode de règlement"
        Me.lbl_ModeRèglementFacture5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_IdentitéClientFacture5
        '
        Me.lbl_IdentitéClientFacture5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_IdentitéClientFacture5.AutoSize = True
        Me.tlp_Facture5.SetColumnSpan(Me.lbl_IdentitéClientFacture5, 2)
        Me.lbl_IdentitéClientFacture5.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_IdentitéClientFacture5.Location = New System.Drawing.Point(4, 69)
        Me.lbl_IdentitéClientFacture5.Name = "lbl_IdentitéClientFacture5"
        Me.lbl_IdentitéClientFacture5.Size = New System.Drawing.Size(495, 40)
        Me.lbl_IdentitéClientFacture5.TabIndex = 14
        Me.lbl_IdentitéClientFacture5.Text = "Référence client"
        Me.lbl_IdentitéClientFacture5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_Facture5
        '
        Me.lbl_Facture5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_Facture5.AutoSize = True
        Me.lbl_Facture5.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Facture5.Location = New System.Drawing.Point(707, 42)
        Me.lbl_Facture5.Name = "lbl_Facture5"
        Me.lbl_Facture5.Size = New System.Drawing.Size(194, 26)
        Me.lbl_Facture5.TabIndex = 12
        Me.lbl_Facture5.Text = "-"
        Me.lbl_Facture5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_Type5
        '
        Me.lbl_Type5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_Type5.AutoSize = True
        Me.lbl_Type5.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Type5.Location = New System.Drawing.Point(506, 42)
        Me.lbl_Type5.Name = "lbl_Type5"
        Me.lbl_Type5.Size = New System.Drawing.Size(194, 26)
        Me.lbl_Type5.TabIndex = 11
        Me.lbl_Type5.Text = "-"
        Me.lbl_Type5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_Référence5
        '
        Me.lbl_Référence5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_Référence5.AutoSize = True
        Me.lbl_Référence5.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Référence5.Location = New System.Drawing.Point(105, 42)
        Me.lbl_Référence5.Name = "lbl_Référence5"
        Me.lbl_Référence5.Size = New System.Drawing.Size(394, 26)
        Me.lbl_Référence5.TabIndex = 10
        Me.lbl_Référence5.Text = "-"
        Me.lbl_Référence5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_MoisFacture5
        '
        Me.lbl_MoisFacture5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_MoisFacture5.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_MoisFacture5.Location = New System.Drawing.Point(917, 1)
        Me.lbl_MoisFacture5.Name = "lbl_MoisFacture5"
        Me.lbl_MoisFacture5.Size = New System.Drawing.Size(93, 40)
        Me.lbl_MoisFacture5.TabIndex = 8
        Me.lbl_MoisFacture5.Text = "Mois"
        Me.lbl_MoisFacture5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_NuméroFacture5
        '
        Me.lbl_NuméroFacture5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_NuméroFacture5.AutoSize = True
        Me.lbl_NuméroFacture5.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_NuméroFacture5.Location = New System.Drawing.Point(707, 1)
        Me.lbl_NuméroFacture5.Name = "lbl_NuméroFacture5"
        Me.lbl_NuméroFacture5.Size = New System.Drawing.Size(194, 40)
        Me.lbl_NuméroFacture5.TabIndex = 7
        Me.lbl_NuméroFacture5.Text = "Facture"
        Me.lbl_NuméroFacture5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_TypeClientFacture5
        '
        Me.lbl_TypeClientFacture5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_TypeClientFacture5.AutoSize = True
        Me.lbl_TypeClientFacture5.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_TypeClientFacture5.Location = New System.Drawing.Point(506, 1)
        Me.lbl_TypeClientFacture5.Name = "lbl_TypeClientFacture5"
        Me.lbl_TypeClientFacture5.Size = New System.Drawing.Size(194, 40)
        Me.lbl_TypeClientFacture5.TabIndex = 6
        Me.lbl_TypeClientFacture5.Text = "Type client"
        Me.lbl_TypeClientFacture5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_NumClientFacture5
        '
        Me.lbl_NumClientFacture5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_NumClientFacture5.AutoSize = True
        Me.lbl_NumClientFacture5.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_NumClientFacture5.Location = New System.Drawing.Point(105, 1)
        Me.lbl_NumClientFacture5.Name = "lbl_NumClientFacture5"
        Me.lbl_NumClientFacture5.Size = New System.Drawing.Size(394, 40)
        Me.lbl_NumClientFacture5.TabIndex = 5
        Me.lbl_NumClientFacture5.Text = "N° Client"
        Me.lbl_NumClientFacture5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_DateFacture5
        '
        Me.lbl_DateFacture5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_DateFacture5.AutoSize = True
        Me.lbl_DateFacture5.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_DateFacture5.Location = New System.Drawing.Point(4, 1)
        Me.lbl_DateFacture5.Name = "lbl_DateFacture5"
        Me.lbl_DateFacture5.Size = New System.Drawing.Size(94, 40)
        Me.lbl_DateFacture5.TabIndex = 4
        Me.lbl_DateFacture5.Text = "Date"
        Me.lbl_DateFacture5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dtp_Date5
        '
        Me.dtp_Date5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtp_Date5.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtp_Date5.Location = New System.Drawing.Point(4, 45)
        Me.dtp_Date5.Name = "dtp_Date5"
        Me.dtp_Date5.Size = New System.Drawing.Size(94, 20)
        Me.dtp_Date5.TabIndex = 9
        '
        'tlp_Facture5
        '
        Me.tlp_Facture5.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.tlp_Facture5.ColumnCount = 5
        Me.tlp_Facture5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.tlp_Facture5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 400.0!))
        Me.tlp_Facture5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200.0!))
        Me.tlp_Facture5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200.0!))
        Me.tlp_Facture5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 118.0!))
        Me.tlp_Facture5.Controls.Add(Me.txb_PrixUnitaire15, 2, 5)
        Me.tlp_Facture5.Controls.Add(Me.lbl_PrixTTCFacture5, 4, 4)
        Me.tlp_Facture5.Controls.Add(Me.lbl_QuantitéHeureFacture5, 3, 4)
        Me.tlp_Facture5.Controls.Add(Me.lbl_PrixUnitaireFacture5, 2, 4)
        Me.tlp_Facture5.Controls.Add(Me.lbl_DésignationFacture5, 1, 4)
        Me.tlp_Facture5.Controls.Add(Me.lbl_CodeFacture5, 0, 4)
        Me.tlp_Facture5.Controls.Add(Me.lbl_ÉchéanceFacture5, 4, 2)
        Me.tlp_Facture5.Controls.Add(Me.lbl_ModeRèglementFacture5, 2, 2)
        Me.tlp_Facture5.Controls.Add(Me.lbl_IdentitéClientFacture5, 0, 2)
        Me.tlp_Facture5.Controls.Add(Me.lbl_Facture5, 3, 1)
        Me.tlp_Facture5.Controls.Add(Me.lbl_Type5, 2, 1)
        Me.tlp_Facture5.Controls.Add(Me.lbl_Référence5, 1, 1)
        Me.tlp_Facture5.Controls.Add(Me.lbl_MoisFacture5, 4, 0)
        Me.tlp_Facture5.Controls.Add(Me.lbl_NuméroFacture5, 3, 0)
        Me.tlp_Facture5.Controls.Add(Me.lbl_TypeClientFacture5, 2, 0)
        Me.tlp_Facture5.Controls.Add(Me.lbl_NumClientFacture5, 1, 0)
        Me.tlp_Facture5.Controls.Add(Me.lbl_DateFacture5, 0, 0)
        Me.tlp_Facture5.Controls.Add(Me.dtp_Date5, 0, 1)
        Me.tlp_Facture5.Controls.Add(Me.cbx_Mois5, 4, 1)
        Me.tlp_Facture5.Controls.Add(Me.lbl_NomPrénom5, 0, 3)
        Me.tlp_Facture5.Controls.Add(Me.lbl_Échéance5, 4, 3)
        Me.tlp_Facture5.Controls.Add(Me.cbx_Règlement5, 2, 3)
        Me.tlp_Facture5.Controls.Add(Me.Label1, 0, 5)
        Me.tlp_Facture5.Controls.Add(Me.Label2, 3, 9)
        Me.tlp_Facture5.Controls.Add(Me.lbl_Prix15, 4, 5)
        Me.tlp_Facture5.Controls.Add(Me.cbx_Objet15, 1, 5)
        Me.tlp_Facture5.Controls.Add(Me.Label3, 3, 5)
        Me.tlp_Facture5.Controls.Add(Me.lbl_TotalTTC5, 4, 9)
        Me.tlp_Facture5.Location = New System.Drawing.Point(2, 5)
        Me.tlp_Facture5.Name = "tlp_Facture5"
        Me.tlp_Facture5.RowCount = 10
        Me.tlp_Facture5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.tlp_Facture5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tlp_Facture5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.tlp_Facture5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tlp_Facture5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.tlp_Facture5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 93.0!))
        Me.tlp_Facture5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.tlp_Facture5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 108.0!))
        Me.tlp_Facture5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 83.0!))
        Me.tlp_Facture5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8.0!))
        Me.tlp_Facture5.Size = New System.Drawing.Size(1010, 607)
        Me.tlp_Facture5.TabIndex = 4
        '
        'txb_PrixUnitaire15
        '
        Me.txb_PrixUnitaire15.Enabled = False
        Me.txb_PrixUnitaire15.Location = New System.Drawing.Point(506, 181)
        Me.txb_PrixUnitaire15.Name = "txb_PrixUnitaire15"
        Me.txb_PrixUnitaire15.Size = New System.Drawing.Size(194, 20)
        Me.txb_PrixUnitaire15.TabIndex = 39
        '
        'cbx_Mois5
        '
        Me.cbx_Mois5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbx_Mois5.FormattingEnabled = True
        Me.cbx_Mois5.Items.AddRange(New Object() {"JANVIER", "FÉVRIER", "MARS", "AVRIL", "MAI", "JUIN", "JUILLET", "AOÛT", "SEPTEMBRE", "OCTOBRE", "NOVEMBRE", "DÉCEMBRE"})
        Me.cbx_Mois5.Location = New System.Drawing.Point(908, 45)
        Me.cbx_Mois5.Name = "cbx_Mois5"
        Me.cbx_Mois5.Size = New System.Drawing.Size(98, 21)
        Me.cbx_Mois5.TabIndex = 13
        '
        'lbl_NomPrénom5
        '
        Me.lbl_NomPrénom5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_NomPrénom5.AutoSize = True
        Me.tlp_Facture5.SetColumnSpan(Me.lbl_NomPrénom5, 2)
        Me.lbl_NomPrénom5.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_NomPrénom5.Location = New System.Drawing.Point(4, 110)
        Me.lbl_NomPrénom5.Name = "lbl_NomPrénom5"
        Me.lbl_NomPrénom5.Size = New System.Drawing.Size(495, 26)
        Me.lbl_NomPrénom5.TabIndex = 19
        Me.lbl_NomPrénom5.Text = "-"
        Me.lbl_NomPrénom5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_Échéance5
        '
        Me.lbl_Échéance5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_Échéance5.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Échéance5.Location = New System.Drawing.Point(917, 110)
        Me.lbl_Échéance5.Name = "lbl_Échéance5"
        Me.lbl_Échéance5.Size = New System.Drawing.Size(93, 26)
        Me.lbl_Échéance5.TabIndex = 21
        Me.lbl_Échéance5.Text = "8 jours"
        Me.lbl_Échéance5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cbx_Règlement5
        '
        Me.tlp_Facture5.SetColumnSpan(Me.cbx_Règlement5, 2)
        Me.cbx_Règlement5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbx_Règlement5.FormattingEnabled = True
        Me.cbx_Règlement5.Items.AddRange(New Object() {"Virement", "Chèque", "CESU", "Chèque + CESU"})
        Me.cbx_Règlement5.Location = New System.Drawing.Point(608, 113)
        Me.cbx_Règlement5.Margin = New System.Windows.Forms.Padding(105, 3, 3, 3)
        Me.cbx_Règlement5.Name = "cbx_Règlement5"
        Me.cbx_Règlement5.Size = New System.Drawing.Size(194, 21)
        Me.cbx_Règlement5.TabIndex = 22
        '
        'Label1
        '
        Me.Label1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(4, 183)
        Me.Label1.Margin = New System.Windows.Forms.Padding(3, 5, 3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(94, 88)
        Me.Label1.TabIndex = 30
        Me.Label1.Text = "Objet :"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label2
        '
        Me.Label2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(707, 566)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(194, 40)
        Me.Label2.TabIndex = 31
        Me.Label2.Text = "TOTAL TTC (€)"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_Prix15
        '
        Me.lbl_Prix15.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_Prix15.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Prix15.Location = New System.Drawing.Point(917, 178)
        Me.lbl_Prix15.Name = "lbl_Prix15"
        Me.lbl_Prix15.Size = New System.Drawing.Size(93, 93)
        Me.lbl_Prix15.TabIndex = 33
        Me.lbl_Prix15.Text = "-"
        Me.lbl_Prix15.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'cbx_Objet15
        '
        Me.cbx_Objet15.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbx_Objet15.FormattingEnabled = True
        Me.cbx_Objet15.Items.AddRange(New Object() {"AIDE A LA PERSONNE", "MENAGE", "LAVAGE/REPASSAGE", "ENTRETIEN PARTIES COMMUNES", "AUTRE"})
        Me.cbx_Objet15.Location = New System.Drawing.Point(105, 181)
        Me.cbx_Objet15.Name = "cbx_Objet15"
        Me.cbx_Objet15.Size = New System.Drawing.Size(264, 21)
        Me.cbx_Objet15.TabIndex = 37
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Georgia", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(707, 178)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(194, 23)
        Me.Label3.TabIndex = 38
        Me.Label3.Text = "FORFAIT"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_TotalTTC5
        '
        Me.lbl_TotalTTC5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbl_TotalTTC5.Font = New System.Drawing.Font("Georgia", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_TotalTTC5.Location = New System.Drawing.Point(917, 571)
        Me.lbl_TotalTTC5.Name = "lbl_TotalTTC5"
        Me.lbl_TotalTTC5.Size = New System.Drawing.Size(93, 29)
        Me.lbl_TotalTTC5.TabIndex = 32
        Me.lbl_TotalTTC5.Text = "-"
        Me.lbl_TotalTTC5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'win_CréationForfait
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1015, 661)
        Me.Controls.Add(Me.btn_Génération5)
        Me.Controls.Add(Me.tlp_Facture5)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "win_CréationForfait"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Système de facturation - Création d'un forfait"
        Me.tlp_Facture5.ResumeLayout(False)
        Me.tlp_Facture5.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btn_Génération5 As Button
    Friend WithEvents lbl_PrixTTCFacture5 As Label
    Friend WithEvents lbl_QuantitéHeureFacture5 As Label
    Friend WithEvents lbl_PrixUnitaireFacture5 As Label
    Friend WithEvents lbl_DésignationFacture5 As Label
    Friend WithEvents lbl_CodeFacture5 As Label
    Friend WithEvents lbl_ÉchéanceFacture5 As Label
    Friend WithEvents lbl_ModeRèglementFacture5 As Label
    Friend WithEvents tlp_Facture5 As TableLayoutPanel
    Friend WithEvents lbl_IdentitéClientFacture5 As Label
    Friend WithEvents lbl_Facture5 As Label
    Friend WithEvents lbl_Type5 As Label
    Friend WithEvents lbl_Référence5 As Label
    Friend WithEvents lbl_MoisFacture5 As Label
    Friend WithEvents lbl_NuméroFacture5 As Label
    Friend WithEvents lbl_TypeClientFacture5 As Label
    Friend WithEvents lbl_NumClientFacture5 As Label
    Friend WithEvents lbl_DateFacture5 As Label
    Friend WithEvents dtp_Date5 As DateTimePicker
    Friend WithEvents cbx_Mois5 As ComboBox
    Friend WithEvents lbl_NomPrénom5 As Label
    Friend WithEvents lbl_Échéance5 As Label
    Friend WithEvents cbx_Règlement5 As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents lbl_TotalTTC5 As Label
    Friend WithEvents lbl_Prix15 As Label
    Friend WithEvents cbx_Objet15 As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txb_PrixUnitaire15 As TextBox
End Class

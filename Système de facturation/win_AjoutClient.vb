﻿Public Class win_AjoutClient
    Private Sub cbx_Type3_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbx_Type3.SelectedIndexChanged
        If cbx_Type3.SelectedItem = "Professionel" Then
            lbl_SociétéClient3.Visible = True
            txb_Société3.Visible = True
        Else
            lbl_SociétéClient3.Visible = False
            txb_Société3.Clear()
            txb_Société3.Visible = False
        End If
    End Sub

    Private Sub btn_AjouterClient3_Click(sender As Object, e As EventArgs) Handles btn_AjouterClient3.Click
        Dim Nom As String = txb_Nom3.Text.ToUpper
        Dim Prénom As String = txb_Prénom3.Text.ToUpper
        Dim Sexe As String = "f"
        If cbx_Sexe3.SelectedItem = "Masculin" Then
            Sexe = "h"
        End If
        Dim Adresse As String = txb_AdresseNuméro3.Text & " " & cbx_AdresseVoie3.SelectedItem & " " & txb_AdresseRue3.Text
        Dim Ville As String = txb_CodePostal3.Text & ", " & txb_Ville3.Text
        Dim Type As String = cbx_Type3.SelectedItem
        Dim Société As String = ""
        If Type = "Professionel" Then
            Société = txb_Société3.Text
        End If
        Dim Référence As String = lbl_Référence3.Text

        If String.IsNullOrWhiteSpace(Nom) Or String.IsNullOrWhiteSpace(Prénom) Or String.IsNullOrWhiteSpace(Sexe) Or String.IsNullOrWhiteSpace(Adresse) Or String.IsNullOrWhiteSpace(Ville) Or String.IsNullOrWhiteSpace(Type) Or String.IsNullOrWhiteSpace(Référence) Then
            MsgBox("Veuillez remplir tous les champs correctement.", vbOKOnly + vbExclamation, "Erreur de remplissage des champs")
        ElseIf Type = "Professionel" And String.IsNullOrWhiteSpace(Société) Then
            MsgBox("Vous avez indiqué que le client est un professionel sans renseigner le noim de la société. Veuillez remplir le champs 'Société' ou définir le client sur 'Particulier'.", vbOKOnly + vbExclamation, "Erreur de remplissage des champs")
        Else
            Dim Requete As String = "INSERT INTO client (sexe, nom, prenom, adresse, ville, type, societe ,num_client, suppr) VALUES ('" & Sexe & "', '" & Nom & "', '" & Prénom & "', '" & Adresse & "', '" & Ville & "', '" & Type & "', '" & Société & "', '" & Référence & "', '0');"
            ConnexionBDD(Requete)
            MsgBox("Le client " & Nom & " " & Prénom & " a été ajouté avec succès !", vbOKOnly + vbInformation, "Confirmation d'ajout du client")
            Me.Close()
            win_GestionClient.Show()
        End If

    End Sub

    Private Sub txb_Nom3_TextChanged(sender As Object, e As EventArgs) Handles txb_Nom3.TextChanged
        If txb_Nom3.Text.Length = 3 Then
            lbl_RéférenceClient3.Visible = True
            lbl_Référence3.Visible = True
            NomClient3 = txb_Nom3.Text.ToUpper
            If DonnéesBDD.Select("num_client='" & "411" & NomClient3 & "'").Length > 0 Then
                Dim i As Integer = 1
                Dim NomClientTemp As String = "411" & NomClient3
                While DonnéesBDD.Select("num_client='" & NomClientTemp & "'").Length > 0
                    NomClientTemp = "411" & NomClient3 & i
                    i = i + 1
                End While
                NomClient3 = NomClientTemp
            Else
                NomClient3 = "411" & txb_Nom3.Text.ToUpper
            End If
            lbl_Référence3.ResetText()
            lbl_Référence3.Text = NomClient3
        ElseIf txb_Nom3.Text.Length < 3 Then
            lbl_Référence3.ResetText()
        End If
    End Sub

    Private Sub win_AjoutClient_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        win_GestionClient.Show()
    End Sub

End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class win_GestionClient
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(win_GestionClient))
        Me.cbx_ListeClients2 = New System.Windows.Forms.ComboBox()
        Me.lbl_ListeClients2 = New System.Windows.Forms.Label()
        Me.gbx_DétailsClient2 = New System.Windows.Forms.GroupBox()
        Me.lbl_Ville2 = New System.Windows.Forms.Label()
        Me.lbl_VilleClient2 = New System.Windows.Forms.Label()
        Me.lbl_Référence2 = New System.Windows.Forms.Label()
        Me.lbl_Société2 = New System.Windows.Forms.Label()
        Me.lbl_Type2 = New System.Windows.Forms.Label()
        Me.lbl_Adresse2 = New System.Windows.Forms.Label()
        Me.lbl_Sexe2 = New System.Windows.Forms.Label()
        Me.lbl_Prénom2 = New System.Windows.Forms.Label()
        Me.lbl_Nom2 = New System.Windows.Forms.Label()
        Me.lbl_RéférenceClient2 = New System.Windows.Forms.Label()
        Me.lbl_NomClient2 = New System.Windows.Forms.Label()
        Me.lbl_PrénomClient2 = New System.Windows.Forms.Label()
        Me.lbl_TypeClient2 = New System.Windows.Forms.Label()
        Me.lbl_SociétéClient2 = New System.Windows.Forms.Label()
        Me.lbl_AdresseClient2 = New System.Windows.Forms.Label()
        Me.lbl_SexeClient2 = New System.Windows.Forms.Label()
        Me.btn_SuppressionClient2 = New System.Windows.Forms.Button()
        Me.btn_ArchivesClient2 = New System.Windows.Forms.Button()
        Me.btn_CréationClient2 = New System.Windows.Forms.Button()
        Me.gbx_DétailsClient2.SuspendLayout()
        Me.SuspendLayout()
        '
        'cbx_ListeClients2
        '
        Me.cbx_ListeClients2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbx_ListeClients2.FormattingEnabled = True
        Me.cbx_ListeClients2.Location = New System.Drawing.Point(58, 222)
        Me.cbx_ListeClients2.Name = "cbx_ListeClients2"
        Me.cbx_ListeClients2.Size = New System.Drawing.Size(148, 21)
        Me.cbx_ListeClients2.TabIndex = 0
        '
        'lbl_ListeClients2
        '
        Me.lbl_ListeClients2.AutoSize = True
        Me.lbl_ListeClients2.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ListeClients2.Location = New System.Drawing.Point(55, 188)
        Me.lbl_ListeClients2.Name = "lbl_ListeClients2"
        Me.lbl_ListeClients2.Size = New System.Drawing.Size(151, 18)
        Me.lbl_ListeClients2.TabIndex = 1
        Me.lbl_ListeClients2.Text = "Liste des clients :"
        '
        'gbx_DétailsClient2
        '
        Me.gbx_DétailsClient2.Controls.Add(Me.lbl_Ville2)
        Me.gbx_DétailsClient2.Controls.Add(Me.lbl_VilleClient2)
        Me.gbx_DétailsClient2.Controls.Add(Me.lbl_Référence2)
        Me.gbx_DétailsClient2.Controls.Add(Me.lbl_Société2)
        Me.gbx_DétailsClient2.Controls.Add(Me.lbl_Type2)
        Me.gbx_DétailsClient2.Controls.Add(Me.lbl_Adresse2)
        Me.gbx_DétailsClient2.Controls.Add(Me.lbl_Sexe2)
        Me.gbx_DétailsClient2.Controls.Add(Me.lbl_Prénom2)
        Me.gbx_DétailsClient2.Controls.Add(Me.lbl_Nom2)
        Me.gbx_DétailsClient2.Controls.Add(Me.lbl_RéférenceClient2)
        Me.gbx_DétailsClient2.Controls.Add(Me.lbl_NomClient2)
        Me.gbx_DétailsClient2.Controls.Add(Me.lbl_PrénomClient2)
        Me.gbx_DétailsClient2.Controls.Add(Me.lbl_TypeClient2)
        Me.gbx_DétailsClient2.Controls.Add(Me.lbl_SociétéClient2)
        Me.gbx_DétailsClient2.Controls.Add(Me.lbl_AdresseClient2)
        Me.gbx_DétailsClient2.Controls.Add(Me.lbl_SexeClient2)
        Me.gbx_DétailsClient2.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbx_DétailsClient2.Location = New System.Drawing.Point(341, 188)
        Me.gbx_DétailsClient2.Name = "gbx_DétailsClient2"
        Me.gbx_DétailsClient2.Size = New System.Drawing.Size(631, 261)
        Me.gbx_DétailsClient2.TabIndex = 2
        Me.gbx_DétailsClient2.TabStop = False
        Me.gbx_DétailsClient2.Text = "Détails client"
        '
        'lbl_Ville2
        '
        Me.lbl_Ville2.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Ville2.Location = New System.Drawing.Point(95, 195)
        Me.lbl_Ville2.Name = "lbl_Ville2"
        Me.lbl_Ville2.Size = New System.Drawing.Size(174, 23)
        Me.lbl_Ville2.TabIndex = 18
        Me.lbl_Ville2.Text = "-"
        '
        'lbl_VilleClient2
        '
        Me.lbl_VilleClient2.AutoSize = True
        Me.lbl_VilleClient2.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_VilleClient2.Location = New System.Drawing.Point(6, 191)
        Me.lbl_VilleClient2.Name = "lbl_VilleClient2"
        Me.lbl_VilleClient2.Size = New System.Drawing.Size(57, 18)
        Me.lbl_VilleClient2.TabIndex = 17
        Me.lbl_VilleClient2.Text = "Ville :"
        '
        'lbl_Référence2
        '
        Me.lbl_Référence2.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Référence2.Location = New System.Drawing.Point(479, 124)
        Me.lbl_Référence2.Name = "lbl_Référence2"
        Me.lbl_Référence2.Size = New System.Drawing.Size(146, 23)
        Me.lbl_Référence2.TabIndex = 16
        Me.lbl_Référence2.Text = "-"
        '
        'lbl_Société2
        '
        Me.lbl_Société2.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Société2.Location = New System.Drawing.Point(479, 77)
        Me.lbl_Société2.Name = "lbl_Société2"
        Me.lbl_Société2.Size = New System.Drawing.Size(146, 23)
        Me.lbl_Société2.TabIndex = 15
        Me.lbl_Société2.Text = "-"
        '
        'lbl_Type2
        '
        Me.lbl_Type2.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Type2.Location = New System.Drawing.Point(479, 37)
        Me.lbl_Type2.Name = "lbl_Type2"
        Me.lbl_Type2.Size = New System.Drawing.Size(146, 23)
        Me.lbl_Type2.TabIndex = 14
        Me.lbl_Type2.Text = "-"
        '
        'lbl_Adresse2
        '
        Me.lbl_Adresse2.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Adresse2.Location = New System.Drawing.Point(95, 152)
        Me.lbl_Adresse2.Name = "lbl_Adresse2"
        Me.lbl_Adresse2.Size = New System.Drawing.Size(174, 23)
        Me.lbl_Adresse2.TabIndex = 13
        Me.lbl_Adresse2.Text = "-"
        '
        'lbl_Sexe2
        '
        Me.lbl_Sexe2.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Sexe2.Location = New System.Drawing.Point(95, 116)
        Me.lbl_Sexe2.Name = "lbl_Sexe2"
        Me.lbl_Sexe2.Size = New System.Drawing.Size(174, 23)
        Me.lbl_Sexe2.TabIndex = 12
        Me.lbl_Sexe2.Text = "-"
        '
        'lbl_Prénom2
        '
        Me.lbl_Prénom2.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Prénom2.Location = New System.Drawing.Point(95, 77)
        Me.lbl_Prénom2.Name = "lbl_Prénom2"
        Me.lbl_Prénom2.Size = New System.Drawing.Size(174, 23)
        Me.lbl_Prénom2.TabIndex = 11
        Me.lbl_Prénom2.Text = "-"
        '
        'lbl_Nom2
        '
        Me.lbl_Nom2.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Nom2.Location = New System.Drawing.Point(95, 37)
        Me.lbl_Nom2.Name = "lbl_Nom2"
        Me.lbl_Nom2.Size = New System.Drawing.Size(174, 23)
        Me.lbl_Nom2.TabIndex = 10
        Me.lbl_Nom2.Text = "-"
        '
        'lbl_RéférenceClient2
        '
        Me.lbl_RéférenceClient2.AutoSize = True
        Me.lbl_RéférenceClient2.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_RéférenceClient2.Location = New System.Drawing.Point(376, 120)
        Me.lbl_RéférenceClient2.Name = "lbl_RéférenceClient2"
        Me.lbl_RéférenceClient2.Size = New System.Drawing.Size(97, 18)
        Me.lbl_RéférenceClient2.TabIndex = 9
        Me.lbl_RéférenceClient2.Text = "Réf client :"
        '
        'lbl_NomClient2
        '
        Me.lbl_NomClient2.AutoSize = True
        Me.lbl_NomClient2.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_NomClient2.Location = New System.Drawing.Point(6, 33)
        Me.lbl_NomClient2.Name = "lbl_NomClient2"
        Me.lbl_NomClient2.Size = New System.Drawing.Size(57, 18)
        Me.lbl_NomClient2.TabIndex = 3
        Me.lbl_NomClient2.Text = "Nom :"
        '
        'lbl_PrénomClient2
        '
        Me.lbl_PrénomClient2.AutoSize = True
        Me.lbl_PrénomClient2.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_PrénomClient2.Location = New System.Drawing.Point(6, 71)
        Me.lbl_PrénomClient2.Name = "lbl_PrénomClient2"
        Me.lbl_PrénomClient2.Size = New System.Drawing.Size(83, 18)
        Me.lbl_PrénomClient2.TabIndex = 4
        Me.lbl_PrénomClient2.Text = "Prénom :"
        '
        'lbl_TypeClient2
        '
        Me.lbl_TypeClient2.AutoSize = True
        Me.lbl_TypeClient2.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_TypeClient2.Location = New System.Drawing.Point(376, 33)
        Me.lbl_TypeClient2.Name = "lbl_TypeClient2"
        Me.lbl_TypeClient2.Size = New System.Drawing.Size(59, 18)
        Me.lbl_TypeClient2.TabIndex = 5
        Me.lbl_TypeClient2.Text = "Type :"
        '
        'lbl_SociétéClient2
        '
        Me.lbl_SociétéClient2.AutoSize = True
        Me.lbl_SociétéClient2.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_SociétéClient2.Location = New System.Drawing.Point(376, 73)
        Me.lbl_SociétéClient2.Name = "lbl_SociétéClient2"
        Me.lbl_SociétéClient2.Size = New System.Drawing.Size(78, 18)
        Me.lbl_SociétéClient2.TabIndex = 6
        Me.lbl_SociétéClient2.Text = "Société :"
        '
        'lbl_AdresseClient2
        '
        Me.lbl_AdresseClient2.AutoSize = True
        Me.lbl_AdresseClient2.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_AdresseClient2.Location = New System.Drawing.Point(6, 148)
        Me.lbl_AdresseClient2.Name = "lbl_AdresseClient2"
        Me.lbl_AdresseClient2.Size = New System.Drawing.Size(83, 18)
        Me.lbl_AdresseClient2.TabIndex = 7
        Me.lbl_AdresseClient2.Text = "Adresse :"
        '
        'lbl_SexeClient2
        '
        Me.lbl_SexeClient2.AutoSize = True
        Me.lbl_SexeClient2.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_SexeClient2.Location = New System.Drawing.Point(6, 109)
        Me.lbl_SexeClient2.Name = "lbl_SexeClient2"
        Me.lbl_SexeClient2.Size = New System.Drawing.Size(56, 18)
        Me.lbl_SexeClient2.TabIndex = 8
        Me.lbl_SexeClient2.Text = "Sexe :"
        '
        'btn_SuppressionClient2
        '
        Me.btn_SuppressionClient2.AutoSize = True
        Me.btn_SuppressionClient2.BackgroundImage = CType(resources.GetObject("btn_SuppressionClient2.BackgroundImage"), System.Drawing.Image)
        Me.btn_SuppressionClient2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_SuppressionClient2.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_SuppressionClient2.ForeColor = System.Drawing.Color.White
        Me.btn_SuppressionClient2.Location = New System.Drawing.Point(58, 290)
        Me.btn_SuppressionClient2.Name = "btn_SuppressionClient2"
        Me.btn_SuppressionClient2.Size = New System.Drawing.Size(180, 37)
        Me.btn_SuppressionClient2.TabIndex = 3
        Me.btn_SuppressionClient2.Text = "Supprimer le client"
        Me.btn_SuppressionClient2.UseVisualStyleBackColor = True
        Me.btn_SuppressionClient2.Visible = False
        '
        'btn_ArchivesClient2
        '
        Me.btn_ArchivesClient2.AutoSize = True
        Me.btn_ArchivesClient2.BackgroundImage = CType(resources.GetObject("btn_ArchivesClient2.BackgroundImage"), System.Drawing.Image)
        Me.btn_ArchivesClient2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_ArchivesClient2.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_ArchivesClient2.ForeColor = System.Drawing.Color.White
        Me.btn_ArchivesClient2.Location = New System.Drawing.Point(58, 346)
        Me.btn_ArchivesClient2.Name = "btn_ArchivesClient2"
        Me.btn_ArchivesClient2.Size = New System.Drawing.Size(180, 37)
        Me.btn_ArchivesClient2.TabIndex = 4
        Me.btn_ArchivesClient2.Text = "Archives du client"
        Me.btn_ArchivesClient2.UseVisualStyleBackColor = True
        Me.btn_ArchivesClient2.Visible = False
        '
        'btn_CréationClient2
        '
        Me.btn_CréationClient2.AutoSize = True
        Me.btn_CréationClient2.BackgroundImage = CType(resources.GetObject("btn_CréationClient2.BackgroundImage"), System.Drawing.Image)
        Me.btn_CréationClient2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_CréationClient2.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_CréationClient2.ForeColor = System.Drawing.Color.White
        Me.btn_CréationClient2.Location = New System.Drawing.Point(410, 130)
        Me.btn_CréationClient2.Name = "btn_CréationClient2"
        Me.btn_CréationClient2.Size = New System.Drawing.Size(180, 37)
        Me.btn_CréationClient2.TabIndex = 5
        Me.btn_CréationClient2.Text = "Ajouter un client"
        Me.btn_CréationClient2.UseVisualStyleBackColor = True
        '
        'win_GestionClient
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(984, 461)
        Me.Controls.Add(Me.btn_CréationClient2)
        Me.Controls.Add(Me.btn_ArchivesClient2)
        Me.Controls.Add(Me.btn_SuppressionClient2)
        Me.Controls.Add(Me.gbx_DétailsClient2)
        Me.Controls.Add(Me.lbl_ListeClients2)
        Me.Controls.Add(Me.cbx_ListeClients2)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "win_GestionClient"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Système de facturation - Gestion des clients"
        Me.gbx_DétailsClient2.ResumeLayout(False)
        Me.gbx_DétailsClient2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cbx_ListeClients2 As ComboBox
    Friend WithEvents lbl_ListeClients2 As Label
    Friend WithEvents gbx_DétailsClient2 As GroupBox
    Friend WithEvents lbl_RéférenceClient2 As Label
    Friend WithEvents lbl_NomClient2 As Label
    Friend WithEvents lbl_PrénomClient2 As Label
    Friend WithEvents lbl_TypeClient2 As Label
    Friend WithEvents lbl_SociétéClient2 As Label
    Friend WithEvents lbl_AdresseClient2 As Label
    Friend WithEvents lbl_SexeClient2 As Label
    Friend WithEvents lbl_Prénom2 As Label
    Friend WithEvents lbl_Nom2 As Label
    Friend WithEvents lbl_Référence2 As Label
    Friend WithEvents lbl_Société2 As Label
    Friend WithEvents lbl_Type2 As Label
    Friend WithEvents lbl_Adresse2 As Label
    Friend WithEvents lbl_Sexe2 As Label
    Friend WithEvents btn_SuppressionClient2 As Button
    Friend WithEvents btn_ArchivesClient2 As Button
    Friend WithEvents btn_CréationClient2 As Button
    Friend WithEvents lbl_Ville2 As Label
    Friend WithEvents lbl_VilleClient2 As Label
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class win_ChoixClient
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(win_ChoixClient))
        Me.cbx_ListeClients4 = New System.Windows.Forms.ComboBox()
        Me.btn_ChoixClient4 = New System.Windows.Forms.Button()
        Me.lbl_ListeClients4 = New System.Windows.Forms.Label()
        Me.lbl_TypeFacture = New System.Windows.Forms.Label()
        Me.cbx_TypeFacture = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'cbx_ListeClients4
        '
        Me.cbx_ListeClients4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbx_ListeClients4.FormattingEnabled = True
        Me.cbx_ListeClients4.Location = New System.Drawing.Point(46, 40)
        Me.cbx_ListeClients4.Name = "cbx_ListeClients4"
        Me.cbx_ListeClients4.Size = New System.Drawing.Size(180, 21)
        Me.cbx_ListeClients4.TabIndex = 0
        '
        'btn_ChoixClient4
        '
        Me.btn_ChoixClient4.AutoSize = True
        Me.btn_ChoixClient4.BackgroundImage = CType(resources.GetObject("btn_ChoixClient4.BackgroundImage"), System.Drawing.Image)
        Me.btn_ChoixClient4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_ChoixClient4.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_ChoixClient4.ForeColor = System.Drawing.Color.White
        Me.btn_ChoixClient4.Location = New System.Drawing.Point(46, 188)
        Me.btn_ChoixClient4.Name = "btn_ChoixClient4"
        Me.btn_ChoixClient4.Size = New System.Drawing.Size(180, 37)
        Me.btn_ChoixClient4.TabIndex = 6
        Me.btn_ChoixClient4.Text = "Créer une facture"
        Me.btn_ChoixClient4.UseVisualStyleBackColor = True
        '
        'lbl_ListeClients4
        '
        Me.lbl_ListeClients4.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ListeClients4.Location = New System.Drawing.Point(46, 10)
        Me.lbl_ListeClients4.Name = "lbl_ListeClients4"
        Me.lbl_ListeClients4.Size = New System.Drawing.Size(180, 18)
        Me.lbl_ListeClients4.TabIndex = 7
        Me.lbl_ListeClients4.Text = "Choisissez un client"
        Me.lbl_ListeClients4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_TypeFacture
        '
        Me.lbl_TypeFacture.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_TypeFacture.Location = New System.Drawing.Point(52, 95)
        Me.lbl_TypeFacture.Name = "lbl_TypeFacture"
        Me.lbl_TypeFacture.Size = New System.Drawing.Size(180, 18)
        Me.lbl_TypeFacture.TabIndex = 9
        Me.lbl_TypeFacture.Text = "Type de facture"
        Me.lbl_TypeFacture.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cbx_TypeFacture
        '
        Me.cbx_TypeFacture.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbx_TypeFacture.FormattingEnabled = True
        Me.cbx_TypeFacture.Items.AddRange(New Object() {"", "Facture classique", "Forfait"})
        Me.cbx_TypeFacture.Location = New System.Drawing.Point(46, 125)
        Me.cbx_TypeFacture.Name = "cbx_TypeFacture"
        Me.cbx_TypeFacture.Size = New System.Drawing.Size(180, 21)
        Me.cbx_TypeFacture.TabIndex = 8
        '
        'win_ChoixClient
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 261)
        Me.Controls.Add(Me.lbl_TypeFacture)
        Me.Controls.Add(Me.cbx_TypeFacture)
        Me.Controls.Add(Me.lbl_ListeClients4)
        Me.Controls.Add(Me.btn_ChoixClient4)
        Me.Controls.Add(Me.cbx_ListeClients4)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "win_ChoixClient"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Système de facturation - Choix d'un client"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cbx_ListeClients4 As ComboBox
    Friend WithEvents btn_ChoixClient4 As Button
    Friend WithEvents lbl_ListeClients4 As Label
    Friend WithEvents lbl_TypeFacture As Label
    Friend WithEvents cbx_TypeFacture As ComboBox
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class win_AjoutClient
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(win_AjoutClient))
        Me.lbl_CodePostalClient3 = New System.Windows.Forms.Label()
        Me.lbl_RéférenceClient3 = New System.Windows.Forms.Label()
        Me.lbl_NomClient3 = New System.Windows.Forms.Label()
        Me.lbl_PrénomClient3 = New System.Windows.Forms.Label()
        Me.lbl_TypeClient3 = New System.Windows.Forms.Label()
        Me.lbl_SociétéClient3 = New System.Windows.Forms.Label()
        Me.lbl_AdresseClient3 = New System.Windows.Forms.Label()
        Me.lbl_SexeClient3 = New System.Windows.Forms.Label()
        Me.lbl_VilleClient3 = New System.Windows.Forms.Label()
        Me.txb_Nom3 = New System.Windows.Forms.TextBox()
        Me.cbx_Type3 = New System.Windows.Forms.ComboBox()
        Me.txb_Prénom3 = New System.Windows.Forms.TextBox()
        Me.cbx_Sexe3 = New System.Windows.Forms.ComboBox()
        Me.txb_AdresseNuméro3 = New System.Windows.Forms.TextBox()
        Me.cbx_AdresseVoie3 = New System.Windows.Forms.ComboBox()
        Me.txb_AdresseRue3 = New System.Windows.Forms.TextBox()
        Me.txb_CodePostal3 = New System.Windows.Forms.TextBox()
        Me.txb_Ville3 = New System.Windows.Forms.TextBox()
        Me.txb_Société3 = New System.Windows.Forms.TextBox()
        Me.lbl_Référence3 = New System.Windows.Forms.Label()
        Me.btn_AjouterClient3 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lbl_CodePostalClient3
        '
        Me.lbl_CodePostalClient3.AutoSize = True
        Me.lbl_CodePostalClient3.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_CodePostalClient3.Location = New System.Drawing.Point(87, 331)
        Me.lbl_CodePostalClient3.Name = "lbl_CodePostalClient3"
        Me.lbl_CodePostalClient3.Size = New System.Drawing.Size(115, 18)
        Me.lbl_CodePostalClient3.TabIndex = 17
        Me.lbl_CodePostalClient3.Text = "Code postal :"
        '
        'lbl_RéférenceClient3
        '
        Me.lbl_RéférenceClient3.AutoSize = True
        Me.lbl_RéférenceClient3.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_RéférenceClient3.Location = New System.Drawing.Point(566, 267)
        Me.lbl_RéférenceClient3.Name = "lbl_RéférenceClient3"
        Me.lbl_RéférenceClient3.Size = New System.Drawing.Size(97, 18)
        Me.lbl_RéférenceClient3.TabIndex = 9
        Me.lbl_RéférenceClient3.Text = "Réf client :"
        Me.lbl_RéférenceClient3.Visible = False
        '
        'lbl_NomClient3
        '
        Me.lbl_NomClient3.AutoSize = True
        Me.lbl_NomClient3.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_NomClient3.Location = New System.Drawing.Point(87, 201)
        Me.lbl_NomClient3.Name = "lbl_NomClient3"
        Me.lbl_NomClient3.Size = New System.Drawing.Size(57, 18)
        Me.lbl_NomClient3.TabIndex = 3
        Me.lbl_NomClient3.Text = "Nom :"
        '
        'lbl_PrénomClient3
        '
        Me.lbl_PrénomClient3.AutoSize = True
        Me.lbl_PrénomClient3.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_PrénomClient3.Location = New System.Drawing.Point(87, 229)
        Me.lbl_PrénomClient3.Name = "lbl_PrénomClient3"
        Me.lbl_PrénomClient3.Size = New System.Drawing.Size(83, 18)
        Me.lbl_PrénomClient3.TabIndex = 4
        Me.lbl_PrénomClient3.Text = "Prénom :"
        '
        'lbl_TypeClient3
        '
        Me.lbl_TypeClient3.AutoSize = True
        Me.lbl_TypeClient3.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_TypeClient3.Location = New System.Drawing.Point(566, 201)
        Me.lbl_TypeClient3.Name = "lbl_TypeClient3"
        Me.lbl_TypeClient3.Size = New System.Drawing.Size(59, 18)
        Me.lbl_TypeClient3.TabIndex = 5
        Me.lbl_TypeClient3.Text = "Type :"
        '
        'lbl_SociétéClient3
        '
        Me.lbl_SociétéClient3.AutoSize = True
        Me.lbl_SociétéClient3.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_SociétéClient3.Location = New System.Drawing.Point(566, 229)
        Me.lbl_SociétéClient3.Name = "lbl_SociétéClient3"
        Me.lbl_SociétéClient3.Size = New System.Drawing.Size(78, 18)
        Me.lbl_SociétéClient3.TabIndex = 6
        Me.lbl_SociétéClient3.Text = "Société :"
        Me.lbl_SociétéClient3.Visible = False
        '
        'lbl_AdresseClient3
        '
        Me.lbl_AdresseClient3.AutoSize = True
        Me.lbl_AdresseClient3.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_AdresseClient3.Location = New System.Drawing.Point(87, 303)
        Me.lbl_AdresseClient3.Name = "lbl_AdresseClient3"
        Me.lbl_AdresseClient3.Size = New System.Drawing.Size(83, 18)
        Me.lbl_AdresseClient3.TabIndex = 7
        Me.lbl_AdresseClient3.Text = "Adresse :"
        '
        'lbl_SexeClient3
        '
        Me.lbl_SexeClient3.AutoSize = True
        Me.lbl_SexeClient3.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_SexeClient3.Location = New System.Drawing.Point(87, 257)
        Me.lbl_SexeClient3.Name = "lbl_SexeClient3"
        Me.lbl_SexeClient3.Size = New System.Drawing.Size(56, 18)
        Me.lbl_SexeClient3.TabIndex = 8
        Me.lbl_SexeClient3.Text = "Sexe :"
        '
        'lbl_VilleClient3
        '
        Me.lbl_VilleClient3.AutoSize = True
        Me.lbl_VilleClient3.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_VilleClient3.Location = New System.Drawing.Point(87, 359)
        Me.lbl_VilleClient3.Name = "lbl_VilleClient3"
        Me.lbl_VilleClient3.Size = New System.Drawing.Size(57, 18)
        Me.lbl_VilleClient3.TabIndex = 18
        Me.lbl_VilleClient3.Text = "Ville :"
        '
        'txb_Nom3
        '
        Me.txb_Nom3.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txb_Nom3.Location = New System.Drawing.Point(226, 202)
        Me.txb_Nom3.Name = "txb_Nom3"
        Me.txb_Nom3.Size = New System.Drawing.Size(297, 25)
        Me.txb_Nom3.TabIndex = 19
        '
        'cbx_Type3
        '
        Me.cbx_Type3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbx_Type3.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbx_Type3.FormattingEnabled = True
        Me.cbx_Type3.Items.AddRange(New Object() {"Particulier", "Professionel"})
        Me.cbx_Type3.Location = New System.Drawing.Point(685, 198)
        Me.cbx_Type3.Name = "cbx_Type3"
        Me.cbx_Type3.Size = New System.Drawing.Size(121, 26)
        Me.cbx_Type3.TabIndex = 20
        '
        'txb_Prénom3
        '
        Me.txb_Prénom3.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txb_Prénom3.Location = New System.Drawing.Point(226, 230)
        Me.txb_Prénom3.Name = "txb_Prénom3"
        Me.txb_Prénom3.Size = New System.Drawing.Size(297, 25)
        Me.txb_Prénom3.TabIndex = 21
        '
        'cbx_Sexe3
        '
        Me.cbx_Sexe3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbx_Sexe3.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbx_Sexe3.FormattingEnabled = True
        Me.cbx_Sexe3.Items.AddRange(New Object() {"Masculin", "Féminin"})
        Me.cbx_Sexe3.Location = New System.Drawing.Point(226, 258)
        Me.cbx_Sexe3.Name = "cbx_Sexe3"
        Me.cbx_Sexe3.Size = New System.Drawing.Size(121, 26)
        Me.cbx_Sexe3.TabIndex = 22
        '
        'txb_AdresseNuméro3
        '
        Me.txb_AdresseNuméro3.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txb_AdresseNuméro3.Location = New System.Drawing.Point(226, 304)
        Me.txb_AdresseNuméro3.Name = "txb_AdresseNuméro3"
        Me.txb_AdresseNuméro3.Size = New System.Drawing.Size(37, 25)
        Me.txb_AdresseNuméro3.TabIndex = 23
        '
        'cbx_AdresseVoie3
        '
        Me.cbx_AdresseVoie3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbx_AdresseVoie3.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbx_AdresseVoie3.FormattingEnabled = True
        Me.cbx_AdresseVoie3.Items.AddRange(New Object() {"Allée", "Avenue", "Boulevard", "Chemin", "Cours", "Esplanade", "Hameau", "Impasse", "Passage", "Place", "Promenade", "Parvis", "Quai", "Route", "Rue", "Square", "Traverse"})
        Me.cbx_AdresseVoie3.Location = New System.Drawing.Point(269, 304)
        Me.cbx_AdresseVoie3.Name = "cbx_AdresseVoie3"
        Me.cbx_AdresseVoie3.Size = New System.Drawing.Size(134, 26)
        Me.cbx_AdresseVoie3.TabIndex = 24
        '
        'txb_AdresseRue3
        '
        Me.txb_AdresseRue3.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txb_AdresseRue3.Location = New System.Drawing.Point(409, 304)
        Me.txb_AdresseRue3.Name = "txb_AdresseRue3"
        Me.txb_AdresseRue3.Size = New System.Drawing.Size(114, 25)
        Me.txb_AdresseRue3.TabIndex = 25
        '
        'txb_CodePostal3
        '
        Me.txb_CodePostal3.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txb_CodePostal3.Location = New System.Drawing.Point(226, 332)
        Me.txb_CodePostal3.MaxLength = 5
        Me.txb_CodePostal3.Name = "txb_CodePostal3"
        Me.txb_CodePostal3.Size = New System.Drawing.Size(121, 25)
        Me.txb_CodePostal3.TabIndex = 26
        '
        'txb_Ville3
        '
        Me.txb_Ville3.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txb_Ville3.Location = New System.Drawing.Point(226, 360)
        Me.txb_Ville3.Name = "txb_Ville3"
        Me.txb_Ville3.Size = New System.Drawing.Size(297, 25)
        Me.txb_Ville3.TabIndex = 27
        '
        'txb_Société3
        '
        Me.txb_Société3.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txb_Société3.Location = New System.Drawing.Point(685, 226)
        Me.txb_Société3.Name = "txb_Société3"
        Me.txb_Société3.Size = New System.Drawing.Size(209, 25)
        Me.txb_Société3.TabIndex = 28
        Me.txb_Société3.Visible = False
        '
        'lbl_Référence3
        '
        Me.lbl_Référence3.Font = New System.Drawing.Font("Georgia", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Référence3.Location = New System.Drawing.Point(682, 267)
        Me.lbl_Référence3.Name = "lbl_Référence3"
        Me.lbl_Référence3.Size = New System.Drawing.Size(100, 23)
        Me.lbl_Référence3.TabIndex = 29
        Me.lbl_Référence3.Visible = False
        '
        'btn_AjouterClient3
        '
        Me.btn_AjouterClient3.AutoSize = True
        Me.btn_AjouterClient3.BackgroundImage = CType(resources.GetObject("btn_AjouterClient3.BackgroundImage"), System.Drawing.Image)
        Me.btn_AjouterClient3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_AjouterClient3.Font = New System.Drawing.Font("Georgia", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_AjouterClient3.ForeColor = System.Drawing.Color.White
        Me.btn_AjouterClient3.Location = New System.Drawing.Point(404, 412)
        Me.btn_AjouterClient3.Name = "btn_AjouterClient3"
        Me.btn_AjouterClient3.Size = New System.Drawing.Size(180, 37)
        Me.btn_AjouterClient3.TabIndex = 30
        Me.btn_AjouterClient3.Text = "Créer le client"
        Me.btn_AjouterClient3.UseVisualStyleBackColor = True
        '
        'win_AjoutClient
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(984, 461)
        Me.Controls.Add(Me.btn_AjouterClient3)
        Me.Controls.Add(Me.lbl_Référence3)
        Me.Controls.Add(Me.txb_Société3)
        Me.Controls.Add(Me.txb_Ville3)
        Me.Controls.Add(Me.txb_CodePostal3)
        Me.Controls.Add(Me.txb_AdresseRue3)
        Me.Controls.Add(Me.cbx_AdresseVoie3)
        Me.Controls.Add(Me.txb_AdresseNuméro3)
        Me.Controls.Add(Me.cbx_Sexe3)
        Me.Controls.Add(Me.txb_Prénom3)
        Me.Controls.Add(Me.cbx_Type3)
        Me.Controls.Add(Me.txb_Nom3)
        Me.Controls.Add(Me.lbl_VilleClient3)
        Me.Controls.Add(Me.lbl_CodePostalClient3)
        Me.Controls.Add(Me.lbl_NomClient3)
        Me.Controls.Add(Me.lbl_SexeClient3)
        Me.Controls.Add(Me.lbl_AdresseClient3)
        Me.Controls.Add(Me.lbl_SociétéClient3)
        Me.Controls.Add(Me.lbl_TypeClient3)
        Me.Controls.Add(Me.lbl_PrénomClient3)
        Me.Controls.Add(Me.lbl_RéférenceClient3)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "win_AjoutClient"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Système de facturation - Ajout d'un client"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lbl_CodePostalClient3 As Label
    Friend WithEvents lbl_RéférenceClient3 As Label
    Friend WithEvents lbl_NomClient3 As Label
    Friend WithEvents lbl_PrénomClient3 As Label
    Friend WithEvents lbl_TypeClient3 As Label
    Friend WithEvents lbl_SociétéClient3 As Label
    Friend WithEvents lbl_AdresseClient3 As Label
    Friend WithEvents lbl_SexeClient3 As Label
    Friend WithEvents lbl_VilleClient3 As Label
    Friend WithEvents txb_Nom3 As TextBox
    Friend WithEvents cbx_Type3 As ComboBox
    Friend WithEvents txb_Prénom3 As TextBox
    Friend WithEvents cbx_Sexe3 As ComboBox
    Friend WithEvents txb_AdresseNuméro3 As TextBox
    Friend WithEvents cbx_AdresseVoie3 As ComboBox
    Friend WithEvents txb_AdresseRue3 As TextBox
    Friend WithEvents txb_CodePostal3 As TextBox
    Friend WithEvents txb_Ville3 As TextBox
    Friend WithEvents txb_Société3 As TextBox
    Friend WithEvents lbl_Référence3 As Label
    Friend WithEvents btn_AjouterClient3 As Button
End Class

﻿Imports System.Data.SqlClient

Public Class win_GestionClient

    Public Sub win_GestionClient_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ' Remplissage dde la liste déroulante.
        ConnexionBDD("SELECT num_client, CONCAT(nom,' ', prenom) AS name FROM client WHERE suppr=0", "ListeClients")
        ConnexionBDD("SELECT * FROM client WHERE suppr=0")

        cbx_ListeClients2.DataSource = DonnéesListe
        cbx_ListeClients2.DisplayMember = "name"
        cbx_ListeClients2.ValueMember = "num_client"



    End Sub

    Private Sub cbx_ListeClients2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbx_ListeClients2.SelectedIndexChanged
        Dim DonnéesClientSelectionnés() As DataRow = DonnéesBDD.Select("num_client='" & cbx_ListeClients2.SelectedValue.ToString() & "'")

        If DonnéesClientSelectionnés.Length <> 0 Then
            btn_SuppressionClient2.Visible = True
            btn_ArchivesClient2.Visible = True

            lbl_Nom2.Text = DonnéesClientSelectionnés(0).ItemArray(2)
            lbl_Prénom2.Text = DonnéesClientSelectionnés(0).ItemArray(3)
            lbl_Adresse2.Text = DonnéesClientSelectionnés(0).ItemArray(5)
            If DonnéesClientSelectionnés(0).ItemArray(1) = "h" Then
                lbl_Sexe2.Text = "Masculin"
            Else
                lbl_Sexe2.Text = "Féminin"
            End If
            lbl_Ville2.Text = DonnéesClientSelectionnés(0).ItemArray(6)
            lbl_Type2.Text = DonnéesClientSelectionnés(0).ItemArray(7)
            If DonnéesClientSelectionnés(0).ItemArray(2) = "Professionel" Then
                lbl_Société2.Text = DonnéesClientSelectionnés(0).ItemArray(4)
            End If
            lbl_Référence2.Text = DonnéesClientSelectionnés(0).ItemArray(8)

        End If



    End Sub

    Private Sub btn_CréationClient2_Click(sender As Object, e As EventArgs) Handles btn_CréationClient2.Click
        win_AjoutClient.Show()
        Me.Hide()
    End Sub

    Private Sub btn_SuppressionClient2_Click(sender As Object, e As EventArgs) Handles btn_SuppressionClient2.Click
        Dim Reponse As Boolean = MsgBox("Êtes-vous sûr de vouloir supprimer le client " & cbx_ListeClients2.SelectedText & " ? Attention sa suppression sera définitive.", vbYesNo + vbCritical, "Demande de suppression définitive")
        If Reponse Then
            ConnexionBDD("UPDATE client SET suppr='1' WHERE num_client='" & cbx_ListeClients2.SelectedValue & "'")
            MsgBox("Le client " & cbx_ListeClients2.SelectedText & " a été supprimé avec succès !", vbOKOnly + vbInformation, "Confirmation de suppression")
        End If
    End Sub

    Private Sub win_GestionClient_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        win_Accueil.Show()
    End Sub
End Class
﻿Imports MySql.Data.MySqlClient

Imports System.IO
Imports System.Text

'iTextSharp Libraries
Imports iTextSharp.text 'Core PDF Text Functionalities
Imports iTextSharp.text.pdf 'PDF Content
Imports iTextSharp.text.pdf.parser 'Content Parser

Module FonctionsGénérales

    Public DonnéesBDD As New DataTable
    Public DonnéesListe As New DataTable
    Public NuméroFacture As New DataTable
    Public DonnéesFacture(35) As String
    Public PrixUnitaireOrigine() As Single = {0, 0, 0, 0}
    Public DonnéesChoixClient() As DataRow
    Public NomClient3 As String

    Function ConnexionBDD(Requete As String, Optional Variable As String = Nothing)

        Dim Connexion As New MySqlConnection("Data Source=cloud.arnaudmallet.fr;Database=facturationAnge;User Id=Malrod;Password=5_DXYKUV_T67-8G;")
        Try
            Connexion.Open()
            Dim cmd As MySqlCommand = New MySqlCommand(Requete, Connexion)
            Dim da As New MySqlDataAdapter
            da.SelectCommand = cmd
            If Variable = "ListeClients" Then
                da.Fill(DonnéesListe)
            ElseIf Variable = "NuméroFacture" Then
                da.Fill(NuméroFacture)
            Else
                da.Fill(DonnéesBDD)
            End If
            da.Dispose()
            Connexion.Close()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Function

    Function GénérationPDF_Forfait(DonnéesFacturation() As String)

        'File.WriteAllBytes(Directory.GetCurrentDirectory, My.Resources.forfait)

        Dim oldFile As String = "C:\Users\Malrod\Documents\Sourcetree\Systeme de facturation\forfait.pdf"
        Dim newFile As String = "C:\Users\Malrod\Documents\Sourcetree\Systeme de facturation\test.pdf"

        If File.Exists(newFile) Then
            My.Computer.FileSystem.DeleteFile(newFile)
        End If

        ' Create reader
        Dim reader As New PdfReader(oldFile)
        Dim size As Rectangle = reader.GetPageSizeWithRotation(1)
        Dim document As New Document(size)

        ' Create the writer
        Dim fs As New FileStream(newFile, FileMode.Create, FileAccess.Write)
        Dim writer As PdfWriter = PdfWriter.GetInstance(document, fs)
        document.Open()
        Dim cb As PdfContentByte = writer.DirectContent

        ' Set the font, color and size properties for writing text to the PDF
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        cb.SetColorFill(BaseColor.DARK_GRAY)
        cb.SetFontAndSize(bf, 8)

        ' Write text in the PDF
        cb.BeginText()
        '######## Définition du texte ########
        ' Adresse de facturation
        Dim client As String = DonnéesFacturation(10)
        Dim adresse As String = DonnéesFacturation(11)
        Dim ville As String = DonnéesFacturation(12)

        ' Première ligne
        Dim ligne1_Date As String = DonnéesFacturation(0)
        Dim ligne1_NumClient As String = DonnéesFacturation(1)
        Dim ligne1_TypeClient As String = DonnéesFacturation(2)
        Dim ligne1_Facture As String = DonnéesFacturation(3)
        Dim ligne1_Mois As String = DonnéesFacturation(4)

        ' Seconde ligne
        Dim ligne2_RefClient As String = DonnéesFacturation(5)
        Dim ligne2_ModeReglement As String = DonnéesFacturation(6)

        ' Facturation
        Dim facturation_Designation As String = "1. " + DonnéesFacturation(7)
        Dim facturation_PrixTTC As String = DonnéesFacturation(8)
        Dim facturation_TotalTTC As String = DonnéesFacturation(9) + "€"
        '#####################################

        ' Set the alignment and coordinates here
        '############## Coordonnées ##############
        ' Adresse de facturation
        cb.ShowTextAligned(0, client, 450, 710, 0)
        cb.ShowTextAligned(0, adresse, 450, 700, 0)
        cb.ShowTextAligned(0, ville, 450, 690, 0)

        ' Première ligne
        cb.ShowTextAligned(1, ligne1_Date, 65, 612, 0)
        cb.ShowTextAligned(1, ligne1_NumClient, 165, 612, 0)
        cb.ShowTextAligned(1, ligne1_TypeClient, 280, 612, 0)
        cb.ShowTextAligned(1, ligne1_Facture, 390, 612, 0)
        cb.ShowTextAligned(1, ligne1_Mois, 500, 612, 0)

        ' Seconde ligne
        cb.ShowTextAligned(1, ligne2_RefClient, 135, 580, 0)
        cb.ShowTextAligned(1, ligne2_ModeReglement, 340, 580, 0)

        ' Facturation
        cb.ShowTextAligned(1, facturation_Designation, 145, 525, 0)
        cb.ShowTextAligned(1, facturation_PrixTTC, 500, 525, 0)
        cb.ShowTextAligned(1, facturation_TotalTTC, 500, 85, 0)

        '#########################################

        cb.EndText()

        ' Put the text on a new page in the PDF 
        Dim page As PdfImportedPage = writer.GetImportedPage(reader, 1)
        cb.AddTemplate(page, 0, 0)

        ' Close the objects
        document.Close()
        fs.Close()
        writer.Close()
        reader.Close()

        MsgBox("Génération forfait effectuée avec succès !")
    End Function

    Function GénérationPDF_Normale(DonnéesFacturation() As String)

        'File.WriteAllBytes(Directory.GetCurrentDirectory, My.Resources.forfait)

        Dim oldFile As String = "C:\Users\Malrod\Documents\Sourcetree\Systeme de facturation\facture.pdf"
        Dim newFile As String = "C:\Users\Malrod\Documents\Sourcetree\Systeme de facturation\test.pdf"

        If File.Exists(newFile) Then
            My.Computer.FileSystem.DeleteFile(newFile)
        End If

        ' Create reader
        Dim reader As New PdfReader(oldFile)
        Dim size As Rectangle = reader.GetPageSizeWithRotation(1)
        Dim document As New Document(size)

        ' Create the writer
        Dim fs As New FileStream(newFile, FileMode.Create, FileAccess.Write)
        Dim writer As PdfWriter = PdfWriter.GetInstance(document, fs)
        document.Open()
        Dim cb As PdfContentByte = writer.DirectContent

        ' Set the font, color and size properties for writing text to the PDF
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        cb.SetColorFill(BaseColor.DARK_GRAY)
        cb.SetFontAndSize(bf, 8)

        ' Write text in the PDF
        cb.BeginText()
        '######## Définition du texte ########
        ' Adresse de facturation
        Dim client As String = DonnéesFacturation(7)
        Dim adresse As String = DonnéesFacturation(8)
        Dim ville As String = DonnéesFacturation(9)

        ' Première ligne
        Dim ligne1_Date As String = DonnéesFacturation(0).Substring(0, 10)
        Dim ligne1_NumClient As String = DonnéesFacturation(1)
        Dim ligne1_TypeClient As String = DonnéesFacturation(2)
        Dim ligne1_Facture As String = DonnéesFacturation(3)
        Dim ligne1_Mois As String = DonnéesFacturation(4)

        ' Seconde ligne
        Dim ligne2_RefClient As String = DonnéesFacturation(5)
        Dim ligne2_ModeReglement As String = DonnéesFacturation(6)

        ' Facturation
        Dim facturation_TotalTTC As String = DonnéesFacturation(10) + " €"

        Dim i As Integer = 11
        Dim j As Integer = 1
        Dim x() As Integer = {100, 100, 100, 280, 390, 500}
        Dim y() As Integer = {525, 515, 505, 525, 525, 525}

        While j <= DonnéesFacturation(35)

            cb.ShowTextAligned(0, j.ToString + " " + DonnéesFacturation(i).ToString, x(0), y(0) - (35 * (j - 1)), 0)
            cb.ShowTextAligned(0, "Période du " + DonnéesFacturation(i + 1).Substring(0, 10), x(1), y(1) - (35 * (j - 1)), 0)
            cb.ShowTextAligned(0, "au " + DonnéesFacturation(i + 2).Substring(0, 10), x(2), y(2) - (35 * (j - 1)), 0)
            cb.ShowTextAligned(1, DonnéesFacturation(i + 3), x(3), y(3) - (35 * (j - 1)), 0)
            cb.ShowTextAligned(1, DonnéesFacturation(i + 4), x(4), y(4) - (35 * (j - 1)), 0)
            cb.ShowTextAligned(1, DonnéesFacturation(i + 5), x(5), y(5) - (35 * (j - 1)), 0)

            i = i + 6
            j = j + 1

        End While

        'cb.ShowTextAligned(0, "1. " + DonnéesFacturation(11), 100, 525, 0)
        ' cb.ShowTextAligned(0, "Période du " + DonnéesFacturation(12).Substring(0, 10), 100, 515, 0)
        'cb.ShowTextAligned(0, " au " + DonnéesFacturation(13).Substring(0, 10), 100, 505, 0)
        ' cb.ShowTextAligned(1, DonnéesFacturation(14), 280, 525, 0)
        '  cb.ShowTextAligned(1, DonnéesFacturation(15), 390, 525, 0)
        '  cb.ShowTextAligned(1, DonnéesFacturation(16), 500, 525, 0)

        '  cb.ShowTextAligned(0, "2. " + DonnéesFacturation(17), 100, 490, 0)
        '  cb.ShowTextAligned(0, "Période du " + DonnéesFacturation(18).Substring(0, 10), 100, 480, 0)
        '  cb.ShowTextAligned(0, " au " + DonnéesFacturation(19).Substring(0, 10), 100, 470, 0)
        '    cb.ShowTextAligned(1, DonnéesFacturation(20), 280, 490, 0)
        '    cb.ShowTextAligned(1, DonnéesFacturation(21), 390, 490, 0)
        '   cb.ShowTextAligned(1, DonnéesFacturation(22), 500, 490, 0)


        cb.ShowTextAligned(1, facturation_TotalTTC, 500, 85, 0)



        '#####################################

        ' Set the alignment and coordinates here
        '############## Coordonnées ##############
        ' Adresse de facturation
        cb.ShowTextAligned(0, client, 450, 710, 0)
        cb.ShowTextAligned(0, adresse, 450, 700, 0)
        cb.ShowTextAligned(0, ville, 450, 690, 0)

        ' Première ligne
        cb.ShowTextAligned(1, ligne1_Date, 65, 612, 0)
        cb.ShowTextAligned(1, ligne1_NumClient, 165, 612, 0)
        cb.ShowTextAligned(1, ligne1_TypeClient, 280, 612, 0)
        cb.ShowTextAligned(1, ligne1_Facture, 390, 612, 0)
        cb.ShowTextAligned(1, ligne1_Mois, 500, 612, 0)

        ' Seconde ligne
        cb.ShowTextAligned(1, ligne2_RefClient, 135, 580, 0)
        cb.ShowTextAligned(1, ligne2_ModeReglement, 340, 580, 0)

        '#########################################

        cb.EndText()

        ' Put the text on a new page in the PDF 
        Dim page As PdfImportedPage = writer.GetImportedPage(reader, 1)
        cb.AddTemplate(page, 0, 0)

        ' Close the objects
        document.Close()
        fs.Close()
        writer.Close()
        reader.Close()

        MsgBox("Génération facture effectuée avec succès !")
    End Function

End Module
